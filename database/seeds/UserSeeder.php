<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
use App\Position;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Schema::disableForeignKeyConstraints('users');
        // User::truncate();
        // Schema::enableForeignKeyConstraints('users');
        

        $adminRole = Role::where('role','admin')->first();
        $userRole = Role::where('role','user')->first();
        $userPosition = Position::all()->random();

        User::create([
            'name'=>'David',
            'role_id'=>$adminRole->id,
            'position_id'=>$userPosition->id,
            'lastname'=>'Consuegra',
            'email'=>'admin@bugtracker.com',
            'password'=>Hash::make('password'),
            'email_verified_at' => now(),
        ]);
        
        $userPosition = Position::all()->random();
        
        User::create([
            'name'=>'User',
            'role_id'=>$userRole->id,
            'position_id'=>$userPosition->id,
            'lastname'=>'User',
            'email'=>'user@bugtracker.com',
            'password'=>Hash::make('password'),
            'email_verified_at' => now(),
        ]);

        factory(User::class)->times(20)->create();
    }
}
