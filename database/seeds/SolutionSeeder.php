<?php

use Illuminate\Database\Seeder;
use App\Solution;

class SolutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Solution::class)->times(800)->create();
    }
}
