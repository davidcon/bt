<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(PositionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ApplicationSeeder::class);
        $this->call(SeveritySeeder::class);
        $this->call(BugSeeder::class);
        $this->call(StepSeeder::class);
        $this->call(SolutionSeeder::class);
    }

    
}
