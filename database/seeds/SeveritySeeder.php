<?php

use Illuminate\Database\Seeder;
use App\Severity;

class SeveritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Severity::create([
            'severity'=>'high',
            'score'=>100,
            'description'=>'This is a high severity level',
        ]);

        Severity::create([
            'severity'=>'medium-high',
            'score'=>75,
            'description'=>'This is a medium-high severity level',
        ]);

        Severity::create([
            'severity'=>'medium',
            'score'=>50,
            'description'=>'This is a medium severity level',
        ]);

        Severity::create([
            'severity'=>'medium-low',
            'score'=>25,
            'description'=>'This is a medium-low severity level',
        ]);

        Severity::create([
            'severity'=>'low',
            'score'=>10,
            'description'=>'This is a low severity level',
        ]);
    }
}
