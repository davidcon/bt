<?php

use Illuminate\Database\Seeder;
use App\Bug;

class BugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Bug::class)->times(1000)->create();
    }
}
