<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bugs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('application_id');
            $table->unsignedBigInteger('severity_id');
            $table->string('location');
            $table->text('description');
            $table->enum('status',['active','pending','solve','inactive'])->default('active');
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
                //   ->onUpdate('cascade')
                //   ->onDelete('No action');

            $table->foreign('application_id')
                  ->references('id')
                  ->on('applications');
                //   ->onUpdate('cascade')
                //   ->onDelete('No action');

            $table->foreign('severity_id')
                  ->references('id')
                  ->on('severities');
                //   ->onUpdate('cascade')
                //   ->onDelete('No action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints('bugs');
        Schema::dropIfExists('bugs');
        Schema::enableForeignKeyConstraints('bugs');
    }
}
