<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bug_id');
            $table->integer('order')->default(1);
            $table->text('description');
            $table->enum('status',['active','inactive'])->default('active');
            $table->timestamps();

            // $table->unique(['bug_id', 'order']);

            $table->foreign('bug_id')
                  ->references('id')
                  ->on('bugs')
                  ->onUpdate('cascade')
                  ->onDelete('No action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints('steps');
        Schema::dropIfExists('steps');
        Schema::enableForeignKeyConstraints('steps');
    }
}
