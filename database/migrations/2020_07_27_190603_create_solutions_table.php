<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solutions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('bug_id');
            $table->text('description');
            $table->string('score')->default(0);
            $table->enum('status',['active','approved','rejected','inactive'])->default('active');
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('No action');

            $table->foreign('bug_id')
                  ->references('id')
                  ->on('bugs')
                  ->onUpdate('cascade')
                  ->onDelete('No action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints('solutions');
        Schema::dropIfExists('solutions');
        Schema::enableForeignKeyConstraints('solutions');
    }
}
