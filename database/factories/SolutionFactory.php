<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Solution;
use App\Bug;
use App\User;
use Faker\Generator as Faker;

$factory->define(Solution::class, function (Faker $faker) {
    $bug = Bug::all()->random();
    return [
        'bug_id'=> $bug->id,
        'user_id'=>User::all()->random(),
        'description'=>$faker->paragraph($nbSentences = 6, $variableNbSentences = true),
        'score'=> $bug->severity->score,
        'status'=>$faker->randomElement(['active','approved','rejected','inactive']),
        'created_at'=>$faker->dateTimeBetween($startDate = $bug->created_at, $endDate = 'now', $timezone = null),
    ];
});
