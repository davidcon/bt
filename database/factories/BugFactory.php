<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Bug;
use App\User;
use App\Application;
use App\Severity;
use Faker\Generator as Faker;

$factory->define(Bug::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random(),
        'application_id' => Application::all()->random(),
        'severity_id' => Severity::all()->random(),
        'location' => $faker->sentence(3),
        'description' => $faker->paragraph,
        'status'=>$faker->randomElement(['active','pending','solve','inactive']),
        'created_at'=>$faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = null),  
    ];
});
