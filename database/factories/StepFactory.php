<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Step;
use Faker\Generator as Faker;
use App\Bug;

$factory->define(Step::class, function (Faker $faker) {
   
    $bug = Bug::all()->random();
    return [
        'bug_id'=>$bug,
        'order'=> $faker->unique($reset = true)->numberBetween($min = 1, $max = 20),
        'description'=>$faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'created_at'=>$faker->dateTimeBetween($startDate = $bug->created_at, $endDate = 'now', $timezone = null),
    ];
});
