@extends('layouts.app')

@section('title','Application - Bugs Details')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 my-3 py-1 bg-white rounded shadow">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="display-5 pl-3 mb-0">@lang('Application - Bugs Details')</h1>
                    <a href="{{ url()->previous() }}" class="btn btn-primary mb-0">@lang('Application List')</a>
                </div>
                <hr>
                <div class="bg-light rounded shadow-sm mx-4 my-2 py-1 px-4">
                    <span class="d-block mb-0">
                        <h5 class="display-5 d-md-inline text-dark font-weight-bold">Application: </h5> 
                        <p class="d-md-inline text-black text-uppercase font-weight-bold">{{ $application->application}}</p>
                    </span>
                    <hr class="my-1">
                    <span>
                        <h5 class="display-5 d-md-inline text-dark font-weight-bold">Description</h5>
                        <p class="text-justify text-dark-50 mb-0">{{ $application->description }}</p>
                    </span>
                </div>
                @include('bug.dinamic_bug_table')
            </div>
        </div>
    </div>
@endsection