@extends('layouts.app')

@section('title','New Application')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form class="bg-light rounded shadow py-3 px-4" 
                      action="{{ route('application.update',$application) }}" method="post">
                      @csrf @method('patch')
                    <h1 class="display-5 pl-3">@lang('Edit Application') {{ $application->application }}</h1>
                    <hr>
                     
                    <div class="form-group">
                        <label for="application">@lang('Application Name')</label>
                        <input name="application"
                            id="application"
                            placeholder="{{ __('Type the application Name') }}" 
                            type="text" 
                            class="form-control shadow-sm @error('application') is-invalid @else border-0 @enderror"
                            value="{{ old('application',$application->application) }}">
                        @error('application')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">@lang('Description')</label>
                        <textarea name="description" 
                                id="description"
                                class="form-control shadow-sm @error('description') is-invalid @else border-0 @enderror"
                                placeholder="{{__('Type a description')}}">{{ old('description',$application->description) }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                    <div class="button-group">
                        <button type="submit" class="btn btn-success">@lang('Save')</button>
                        <a href="{{ route('application.index') }}" class="btn btn-danger">@lang('Cancel')</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection