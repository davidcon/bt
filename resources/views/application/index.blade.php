@extends('layouts.app')

@section('title','Application List')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            <h1 class="display-5 mb-0">@lang('Application List')</h1>
            <div class="btn-group">
                @if(Auth::user()->isAdmin())
                    <a class="btn btn-success rounded mr-2" href="{{ route('application.create') }}">@lang('New Application')</a>
                    <a class="pt-1" href="{{ route('application.applicationExcelExport') }}"><i class="far fa-file-excel fa-2x"></i></a>
                @endif
            </div>
        </div>
        <hr>
        <table id="dinamic-application" 
               class="table table-sm table-bordered table-hover">
            <thead class="bg-white">
                <tr>
                    <th>Application</th>
                    <th>Description</th>
                    <th class="text-center">Bugs</th>
                    @if(Auth::user()->isAdmin())
                        <th class="text-center">Edit</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Del</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @forelse($applications as $application)
                    <tr>
                      <td>{{ $application->application }}</td>
                      <td>{{ $application->description }}</td>
                      <td class="text-center">
                        @if($application->hasBugs())
                          <a href="{{ route('application.show', $application) }}" >
                             <i class="fas fa-bug fa-2x text-secondary"></i>
                          </a>
                        @endif
                      </td>
                      @if(Auth::user()->isAdmin())
                        <td class="text-center"> 
                                <a href="{{ route('application.edit', $application) }}">
                                <i class="far fa-edit fa-2x text-primary"></i>
                            </a>
                        </td>
                        <td class="text-center">
                                <a href="{{ route('application.manage', $application) }}"
                                onclick="event.preventDefault(); 
                                            document.getElementById('change-status-form-{{ $application->id }}').submit();">
                                @if( $application->status == 'active')   
                                        <i class="fas fa-power-off fa-2x text-success"></i>
                                @else
                                        <i class="fas fa-power-off fa-2x text-danger"></i>
                                @endif   
                                </a>
                                <form   class="d-none"
                                        id="change-status-form-{{ $application->id }}"
                                        action="{{ route('application.manage', $application) }}" 
                                        method="post">
                                        @csrf @method('patch')
                                        <input type="hidden" name="status" value=" @if($application->status == 'active') inactive @else active @endif ">
                                </form>
                        </td>
                        <td class="text-center">
                            @if($application->isAllowDelete())
                                <a href="{{ route('application.delete', $application) }}"
                                onclick="event.preventDefault(); 
                                            document.getElementById('change-delete-form-{{ $application->id }}').submit();">
                                    <i class="fas fa-trash-alt fa-2x text-dark"></i>
                                </a>
                                <form class="d-none"
                                    id="change-delete-form-{{ $application->id }}"
                                    action="{{ route('application.delete', $application) }}" 
                                    method="post">
                                    @csrf @method('delete')
                                </form>
                            @endif      
                        </td>
                      @endif
                    </tr>
                @empty
                    <tr>
                      <td>@lang('No applications to dislay')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>

@endsection