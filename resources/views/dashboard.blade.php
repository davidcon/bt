@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="col-12 bg-white rounded shadow py-1 mx-1 my-2">
                @include('dashboard.applications_t5')
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="col-12 bg-white rounded shadow py-1 mx-1 my-2">
                @if(Auth::user()->isAdmin())
                    @include('dashboard.users_t5')
                @else
                    @include('dashboard.users_m5')
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="col-12 bg-white rounded shadow py-1 mx-1 my-2">
                <h6 class="text-center"><strong>Latest Bugs</strong></h6>
                <table class="table table-sm table-hover">
                    <thead class="bg-warning">
                        <tr>
                            <th>Bug</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($bugs as $bug) 
                        <tr>
                            <td>
                                <a href="{{ route('bug.show', $bug) }}" 
                                   class="text-decoration-none text-secondary">
                                   {{ $bug->location }}
                                </a>
                            </td>
                            <td>{{ $bug->created_at->format('m-d-Y') }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="col-12 bg-white rounded shadow py-1 mx-1 my-2">
                <h6 class="text-center"><strong>Bugs Severity table - {{ date('Y')}} </strong></h6>
                <table class="table table-sm table-hover">
                    <thead class="bg-secondary">
                        <tr>
                            <th>Severity</th>
                            <th>Bugs</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($severities as $severity) 
                        <tr>
                            <td>{{ $severity->severity }}</td>
                            <td>{{ $severity->bugs_count }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
