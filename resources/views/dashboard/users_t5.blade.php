<h6 class="text-center"><strong>Users - T5</strong></h6>
<table class="table table-sm table-hover">
    <thead class="bg-success">
        <tr>
            <th>User</th>
            <th>Score</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user) 
            <tr>
                <td>{{ $user->name }} {{ $user->lastname }}</td>
                <td>
                    <i class="fas fa-trophy fa-2x text-warning"></i>
                    <span class="h3">{{ $user->user_score }}</span> 
                </td>
            </tr>
        @endforeach
    </tbody>
</table>