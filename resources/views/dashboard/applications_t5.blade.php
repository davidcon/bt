<h6 class="text-center"><strong>Applications - T5</strong></h6>
                <table class="table table-sm table-hover">
                    <thead class="bg-primary">
                        <tr>
                            <th>Application</th>
                            <th>Bugs</th>
                            <th>Det</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($applications as $application) 
                        <tr>
                            <td>{{ $application->application }}</td>
                            <td>{{ $application->bugs_count }}</td>
                            <td><a href="{{ route('application.show',$application)}}"
                                class="text-decoration-none">
                                    <i class="fas fa-info-circle fa-2x"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>