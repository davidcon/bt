<h6 class="text-center">
    <a href="{{ route('user.edit',$users->first()->id)}}"
       class="text-secondary font-weight-bold text-decoration-none">
        {{ $users->first()->name }} {{ $users->first()->lastname }}- 5M
    </a>
</h6>
<table class="table table-sm table-hover">
    <thead class="bg-success">
        <tr>
            <th>Date(Year - Month)</th>
            <th>Score</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user) 
            <tr>
                <td>{{ $user->year }} - {{ $user->month }}</td>
                <td>
                    <i class="fas fa-trophy fa-2x text-warning"></i>
                    <span class="h3">{{ $user->user_score }}</span> 
                </td>
            </tr>
        @endforeach
    </tbody>
</table>