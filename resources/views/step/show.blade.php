<div class="col-12 bg-light rounded shadow py-1 mx-1 my-2">
    <table id="dinamic-steps" 
           class="table table-sm table-striped table-bordered table-hover" 
           style="width:100%">
        <thead class="bg-white">
            <tr>
                <th>No.</th>
                <th>Steps</th>
            </tr>
        </thead>
        <tbody>
            @foreach($bug->steps as $step) 
                <tr>
                    <td>{{ $step->order }}</td>
                    <td>{{ $step->description }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>