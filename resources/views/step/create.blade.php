@extends('layouts.app')

@section('title','Add steps to a Bug ')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <div class="bg-white rounded px-3 py-2">
                    <div class="d-flex justify-content-between align-items-center mt-2">
                        <h1 class="display-5 mb-0">@lang('Add steps to a Bug')</h1>
                        <a href="{{ route('application.index') }}" class="btn btn-primary mb-0">@lang('Application List')</a>
                    </div>
                    <hr class="my-2">
                    <div class="bg-light rounded shadow-sm my-2 py-1 px-2">
                        <span class="d-block mb-0">
                            <h5 class="display-5 d-md-inline text-dark font-weight-bold">Application: </h5> 
                            <p class="d-md-inline text-black text-uppercase font-weight-bold">{{ $step->bug->application->application }}</p>
                        </span>
                        <hr class="my-1">
                        <span>
                            <h5 class="display-5 d-md-inline text-dark font-weight-bold">Description</h5>
                            <p class="text-justify text-dark-50 mb-0">{{ $step->bug->application->description }}</p>
                        </span>
                    </div>
                    <div class="bg-light rounded shadow-sm my-2 py-1 px-2">
                        <span class="d-block mb-0">
                            <h5 class="display-5 d-md-inline text-dark font-weight-bold">Bug: </h5> 
                            <p class="d-md-inline text-black text-uppercase font-weight-bold">{{ $step->bug->location }}</p>
                        </span>
                        <hr class="my-1">
                        <span>
                            <h5 class="display-5 d-md-inline text-dark font-weight-bold">Description</h5>
                            <p class="text-justify text-dark-50 mb-0">{{ $step->bug->description }}</p>
                        </span>
                    </div>              
                </div>
            </div>
      
            <div class="col-sm-12 col-lg-6">
                <div class="bg-white rounded px-3 py-2">
                    <form action="{{ route('step.store') }}" method="post" 
                          class="bg-light py-2 rounded">
                        @csrf
                        <div class="form-group px-2">
                            <label for="description"><strong>@lang('Add a Step Description')</strong></label>
                            <textarea name="description"
                                      placeholder="Type de step description" 
                                      class="form-control shadow-sm 
                                           @error('description') is-invalid  @else border-0 @enderror">{{ old('description')}}</textarea>
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    {{ $message }}
                                </span>
                            @enderror
                            <input type="hidden" name="bug_id" value="{{ $step->bug->id }}">
                        </div>
                        <div class="buttom-group px-2">
                            <button type="submit" class="btn btn-success">@lang('Save')</button>
                            <a href="{{ route('bug.index') }}" class="btn btn-danger">@lang('Cancel')</a>
                        </div>
                    </form>
                    @include('bug.dinamic_bug_steps')
                </div>     
            </div>      
        </div>
    </div>
@endsection