@extends('layouts.app')

@section('title','Severity List')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="display-5 mb-0">@lang('Severity List')</h1>
                    <a class="btn btn-success" href="{{ route('severity.create') }}">@lang('New Severity')</a>
                </div>
                <hr>
                <ul class="list-group">
                    @forelse($severities as $severity)
                    <li class="list-group-item border-0 bg-light shadow-sm mb-3">
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <p>
                                    <span class="text-uppercase font-weight-bold text-secondary">{{ $severity->severity }}</span> 
                                    <span class="ml-3"> Descriptio : {{ $severity->description }} </span>
                                </p>
                            </div>
                            <div>
                                <a href="{{ route('severity.edit', $severity ) }}" 
                                   class="btn btn-sm btn-primary mb-1">Edit</a>
                                <a href="{{ route('severity.update', $severity) }}"
                                   onclick="event.preventDefault();
                                            document.getElementById('change-status-form-{{ $severity->id }}').submit();"
                                @if($severity->status == 'active')
                                    class="btn btn-sm btn-warning mb-1">Inactive
                                @else
                                    class="btn btn-sm btn-success mb-1">Active
                                @endif
                                </a>
                                <form action="{{ route('severity.update', $severity) }}" 
                                      id="change-status-form-{{ $severity->id }}"
                                      method="post" class="d-none" >
                                      @csrf @method('patch')
                                      <input type="hidden" name="severity" value="{{ $severity->severity }}">
                                      <input type="hidden" name="score" value="{{ $severity->score }}">
                                      <input type="hidden" name="description" value="{{ $severity->description }}">
                                      <input type="hidden" name="status" 
                                             value="@if($severity->status == 'active') inactive @else active @endif">
                                </form>
                                <a href="{{ route('severity.delete', $severity) }}" 
                                   class="btn btn-sm btn-danger mb-1"
                                   onclick="event.preventDefault();
                                            document.getElementById('delete-form-{{ $severity->id }}').submit()">Delete</a>
                                <form action="{{ route('severity.delete', $severity) }}"
                                      id="delete-form-{{ $severity->id }}" 
                                      method="post" class="d-none">
                                      @csrf @method('delete')
                                </form>
                            </div>
                        </div>
                    </li>
                    @empty
                        <li class="list-group-item border-0 bg-light shadow-sm mb-3">@lang('There is no Severity to display')</li>
                    @endforelse
                </ul>
                {{ $severities->links() }}
            </div>
        </div>
    </div>
@endsection