@extends('layouts.app')

@section('title','New Severity')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('severity.store') }}" 
                      method="post" 
                      class="bg-light rounded shadow py-3 px-4">
                      @csrf
                    <h1 class="display-5 pl-3">@lang('New Severity')</h1>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-9">
                            <label for="severity">@lang('Severity')</label>
                            <input name="severity"
                                id="severity"
                                placeholder="{{ __('Type the severity Name') }}" 
                                type="text" 
                                class="form-control  shadow-sm @error('severity') is-invalid @else border-0 @enderror"
                                value="{{ old('severity') }}">
                            @error('severity')
                                <span class="invalid-feedback" role="alert">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="score">@lang('Score')</label>
                            <input type="number" min="0" max="100"
                                name="score" value="10"
                                id="score" 
                                class="form-control shadow-sm @error('points') is-invalid @else border-0 @enderror">
                            @error('score')
                                <span class="invalid-feedback" role="alert">
                                    {{ $message }}
                                </span>
                            @enderror       
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">@lang('Description')</label>
                        <textarea name="description"
                                  class="form-control shadow-sm @error('description') is-invalid @else border-0 @enderror"
                                  placeholder="{{__('Type a description')}}">{{ old('description') }}</textarea>
                        @error('description')
                            <span class="invalid-feedbak" role="alert">
                                {{ $message }}
                            </span>
                        @enderror          
                    </div>
                    <div class="button-group">
                        <button type="submit" class="btn btn-success">@lang('Save')</button>
                        <a href="{{ route('severity.index') }}" class="btn btn-danger">@lang('Cancel')</a>
                    </div>    
                </form>
            </div>
        </div>
    </div>
@endsection