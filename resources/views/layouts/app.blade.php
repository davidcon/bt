<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    <link rel="shortcut icon" href="/img/logo.ico" type="image/x-icon">
    <!-- Scripts -->
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://kit.fontawesome.com/5c4258676e.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
</head>
<body>
    <div id="app">
        <header class="fixed-top">
            @include('partials._nav')
            @include('partials.session-status') 
        </header>

        <main class="mt-5 py-5">
            @yield('content')
        </main>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" defer></script>
    <script>
        $(document).ready(function() {

            $('#dinamic-bugs').DataTable({
                "pageLength": 5,
                "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
                // "processing": true,
                // "serverSide": true,
                // "ajax": "scripts/server_processing.php"  
            });

            $('#dinamic-users').DataTable({
                "pageLength": 7,
                "lengthMenu": [[7,10, 25, 50, -1], [5,10, 25, 50, "All"]]  
            });

            $('#dinamic-application').DataTable({ 
                "pageLength": 5,
                "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
                // "processing": true,
                // "serverSide": true,
                // "ajax": "/application",
                // "columns":[
                //     {data: 'application'},
                //     {data: 'description'},
                // ]  
            });

            $('#dinamic-steps').DataTable({ 
                "pageLength": 2,
                "lengthMenu": [[2,5,10, 25, 50, -1], [2,5,10, 25, 50, "All"]]  
            });

            
        } );
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>  
</body>
</html>
