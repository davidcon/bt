@extends('layouts.app')

@section('title','Bug List')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="display-5 mb-0">@lang('Bug List')</h1>
                    <div class="btn-group">
                        <a href="{{ route('bug.create') }}" class="btn btn-success rounded mr-2">@lang('New Bug')</a>
                        @if(Auth::user()->isAdmin())
                            <a class="pt-1" href="{{ route('bug.bugExcelExport') }}"><i class="far fa-file-excel fa-2x"></i></a>
                        @endif
                    </div>
                    
                </div>
                <hr>
                <table id="dinamic-bugs" 
                        class="table table-sm table-striped table-bordered table-hover" 
                        style="width:100%">
                        <thead class="bg-white">
                            <tr>
                                <th>Application</th>
                                <th>Location</th>
                                <th>Description</th>
                                <th>Details</th>
                                <th>Edit</th>
                                <th>Del</th>
                                @if(Auth::user()->isAdmin())
                                    <th class="text-center">Manage</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bugs as $bug) 
                                <tr>
                                    <td>{{ $bug->getApplicationName() }}</td>
                                    <td>{{ $bug->location }}</td>
                                    <td>{{ $bug->description }}</td>
                                    <td class="text-center">
                                        @if($bug->status == 'pending')
                                            <a href="{{ route('bug.show', $bug) }}">
                                                <i class="fas fa-info-circle fa-2x text-primary"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($bug->allowEdit())
                                            <a href="{{ route('bug.edit', $bug) }}">
                                                <i class="far fa-edit fa-2x text-primary"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($bug->allowDelete())
                                            <a href="{{ route('bug.delete', $bug) }}"
                                               onclick="event.preventDefault();
                                                        document.getElementById('delete-form-{{ $bug->id }}').submit()">
                                                <i class="fas fa-trash-alt fa-2x text-danger"></i>
                                            </a>
                                            <form class="d-none"
                                                id="delete-form-{{ $bug->id }}"
                                                action="{{ route('bug.delete', $bug) }}" 
                                                method="post">
                                                @csrf @method('delete')
                                            </form>
                                        @endif
                                    </td>
                                    @if(Auth::user()->isAdmin())
                                    <td class="text-center">
                                        @if($bug->status == 'active')
                                            <a href="{{ route('bug.manage', $bug) }}">
                                                <i class="fas fa-tasks fa-2x text-dark"></i>
                                            </a>
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>      
    </div>
@endsection