@extends('layouts.app')

@section('title','Bug Detail')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="bg-white rounded shadow px-4 py-3">
                    <div class="d-flex justify-content-between align-items-center mb-0">
                        <h1 class="display-5 mb-0">
                            Bug Detail
                        </h1>
                        <a href="{{ route('bug.index') }}" class="btn btn-primary mb-1">
                            Bug List
                        </a>
                    </div>
                    <hr class="my-0 py-1">
                    <div class="col-12 col-md-6 my-0 py-0 d-md-inline">
                        <h5 class="text-secondary d-inline font-weight-bold my-0 pb-1">Application: </h5>
                        <p class="text-secondary d-md-inline-block my-0 pb-1">{{ $bug->getApplicationName() }}</p>
                    </div>
                    <div class="col-12 col-md-6 my-0 py-0 d-md-inline-block">
                        <h5 class="text-secondary d-inline font-weight-bold my-0 pb-1">Location: </h5>
                        <p class="text-secondary d-md-inline-block my-0 pb-1">{{ $bug->location }}</p>
                    </div>
                    <div class="col-12 my-0 py-0">
                        <h5 class="text-secondary d-inline font-weight-bold my-0 pb-1">Bug: </h5>
                        <p class="text-secondary d-md-inline my-0 pb-1">{{ $bug->description  }}</p>
                    </div>
                    <hr class="my-0 py-1">
                    <div class="col-12 my-0 py-1">
                        <table id="dinamic-steps" 
                            class="table table-sm table-striped table-bordered table-hover text-decoration-none" 
                            style="width:100%">
                            <thead class="bg-white">
                                <tr>
                                    <th>No.</th>
                                    <th>Steps</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bug->getBugsteps() as $step) 
                                    <tr>
                                        <td>{{ $step->order }}</td>
                                        <td>{{ $step->description }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr class="my-0 py-1">
                    @if($solution) 
                        @include('solution.edit')
                    @else
                        @include('solution.create')
                    @endif
                </div>
            </div>
        </div>     
    </div>    
@endsection