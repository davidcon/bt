@extends('layouts.app')

@section('title','Bug Management')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="bg-white rounded shadow px-4 py-3">
                    <div class="d-flex justify-content-between align-items-center mb-0">
                        <h1 class="display-5 mb-0">
                            Bug Management
                        </h1>
                        <a href="{{ route('bug.index') }}" class="btn btn-primary mb-0">
                            Bug List
                        </a>
                    </div>
                    <hr class="my-0 py-1">
                    <div class="col-12 my-0 py-0">
                        <h5 class="text-secondary d-inline font-weight-bold my-0 pb-1">Application: </h5>
                        <p class="text-secondary d-md-inline-block my-0 pb-1">{{ $bug->getApplicationName() }}</p>
                    </div>
                    <div class="col-12 my-0 py-0">
                        <h5 class="text-secondary d-inline font-weight-bold my-0 pb-1">Location: </h5>
                        <p class="text-secondary d-md-inline-block my-0 pb-1">{{ $bug->location }}</p>
                    </div>
                    <div class="col-12 my-0 py-0">
                        <h5 class="text-secondary d-inline font-weight-bold my-0 pb-1">Bug: </h5>
                        <p class="text-secondary d-md-inline my-0 pb-1">{{ $bug->description  }}</p>
                    </div>
                    <hr class="my-0 py-1">
                    <div class="col-12 my-0 py-1">
                        <table id="dinamic-steps" 
                            class="table table-sm table-striped table-bordered table-hover text-decoration-none" 
                            style="width:100%">
                            <thead class="bg-white">
                                <tr>
                                    <th>No.</th>
                                    <th>Steps</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bug->getBugsteps() as $step) 
                                    <tr>
                                        <td>{{ $step->order }}</td>
                                        <td>{{ $step->description }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr class="my-0 py-1">
                    <div class="col-12 my-0 py-1">
                        <div class="button-group">
                            <a href="{{ route('bug.managestatus',$bug) }}" 
                               class="btn btn-success col-sm-12 col-md-1 btn-block  d-md-inline mx-1"
                               onclick="document.getElementById('status').value = 'pending'; 
                                        event.preventDefault();
                                        document.getElementById('change-status-form').submit();">
                            Approved
                            </a>
                            <a href="{{ route('bug.managestatus',$bug) }}" 
                               class="btn btn-danger col-sm-12 col-md-1 btn-block d-md-inline"
                               onclick="document.getElementById('status').value = 'inactive';
                                        event.preventDefault();
                                        document.getElementById('change-status-form').submit();">
                            Rejected
                            </a>
                        </div>
                        <form 
                                action="{{ route('bug.managestatus',$bug) }}" 
                                method="post"
                                id="change-status-form"
                                class="d-none">
                            @csrf @method('patch')
                            <input type="hidden" name="status" id="status">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection