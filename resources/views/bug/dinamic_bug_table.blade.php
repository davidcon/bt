<div class="bg-white px-4 py-2">
    <table id="dinamic-bugs" 
        class="table table-sm table-bordered table-hover" 
        style="width:100%">
        <thead class="bg-light">
            <tr>
                <th>Location</th>
                <th>Description</th>
                <th>Status</th>
                <th>Details</th>
                @if(Auth::user()->isAdmin())
                    <th>Manage</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($application->getBugs() as $bug) 
                <tr>
                    <td class="text-secondary">
                            {{ $bug->location }}
                    </td>
                    <td class="text-secondary">
                            {{ $bug->description }}
                    </td>
                    <td class="text-secondary">
                        {{ $bug->status }}
                    </td>
                    <td class="text-center">
                        @if($bug->status == 'pending')
                            <a href="{{ route('bug.show', $bug) }}">
                                <i class="far fa-edit fa-2x text-primary"></i>
                            </a>
                        @endif
                    </td>
                    @if(Auth::user()->isAdmin())
                        <td class="text-center">
                            @if($bug->status == 'active')
                                <a href="{{ route('bug.manage', $bug) }}">
                                    <i class="fas fa-tasks fa-2x text-dark"></i>
                                </a>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>