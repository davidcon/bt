<div class="col-12 bg-light rounded py-1 my-2">
    <table id="dinamic-steps" 
           class="table table-sm table-striped table-bordered table-hover" 
           style="width:100%">
        <thead class="bg-white">
            <tr>
                <th>No.</th>
                <th>Step</th>
            </tr>
        </thead>
        <tbody>
            @foreach($step->bug->steps as $step) 
                <tr>
                    <td class="text-center">{{ $step->order }}</td>
                    <td class="text-justify">{{ $step->description }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>