@extends('layouts.app')

@section('title','Edit Bug')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('bug.update', $bug
                ) }}" 
                      method="post" 
                      class="bg-light rounded shadow py-3 px-4">
                    @csrf @method('patch')
                    <h1 class="display-5">Edit Bug</h1>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="application_id">@lang('Application')</label>
                            <select name="application_id"
                                    class="form-control shadow-sm
                                           @error('application_id') is-invalid @else border-0 @enderror ">
                                    <option value="">Choose an application...</option>
                                @forelse($applications as $application )
                                    <option 
                                        value="{{ $application->id }}"
                                        @if($application->id == old('application_id',$bug->application_id )) selected @endif>{{ $application->application }}</option>
                                @empty
                                    <option>There is no application list</option>
                                @endforelse
                            </select>
                            @error('application_id')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="severity_id">@lang('Severity')</label>
                            <select name="severity_id"
                                    class="form-control shadow-sm 
                                           @error('severity_id') is-invalid @else border-0 @enderror">
                                <option value="" selected>Choose an severity...</option>
                                @forelse($severities as $severity )
                                    <option 
                                           value="{{ $severity->id }}" 
                                           @if($severity->id == old('severity_id',$bug->severity_id)) selected @endif>{{ $severity->severity }}</option>
                                @empty
                                    <option>There is no severity list</option>
                                @endforelse
                            </select>
                            @error('severity_id')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="bug">@lang('Bug Location')</label>
                            <input type="text" name="location" id="location"
                                placeholder="Type the location of the bug" 
                                class="form-control shadow-sm @error('location') is-invalid @else border-0 @enderror"
                                value="{{ old('location',$bug->location)}}">
                            @error('location')
                                <span class="invalid-feedback" role="alert">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="description">@lang('Description')</label>
                        <textarea name="description" class="form-control shadow-sm @error('description') is-invalid @else border-0 @enderror"
                        placeholder="Type a description of the bug">{{ old('description',$bug->description) }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                    <div class="button-group">
                        <button type="submit" class="btn btn-success">@lang('Update')</button>
                        <a href="{{ route('bug.index') }}" class="btn btn-danger">@lang('Cancel')</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection