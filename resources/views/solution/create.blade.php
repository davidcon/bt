<div class="col-12 my-0 py-0">
    <form action="{{ route('solution.store')}}" 
          method="post">
          @csrf
        <div class="d-flex justify-content-between align-items-center">
            <h3 class="display-5 mb-0">@lang('Solution')</h3>
            <button class="btn btn-success rounded mb-1">@lang('Save')</button>
        </div>                       
        <hr class="my-0 py-1">
        <div class="form-group mx-0 px-0">
            <label for="description">@lang('Describe your solution')</label>
            <textarea name="description" id="description" 
                      class="form-control bg-light rounded shadow-sm 
                             @error('description') is-invalid @else border-0 @enderror"
                      placeholder="Type the desription of the solution">{{ old('description')}}</textarea>
            @error('description')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
            @enderror
        </div>
        <input type="hidden" name="bug_id" value="{{ $bug->id }}">
    </form>
</div>