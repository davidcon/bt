    <div class="col-12 my-0 py-0">
        <form action="{{ route('solution.update', $solution) }}" 
              method="post">
              @csrf @method('patch')
                <div class="d-flex justify-content-between align-items-center">
                    <h3 class="display-5 mb-0">@lang('Solution')</h3>
                    <div class="button-group mb-1">
                        @if($solution->status == 'active')
                            <button class="btn btn-success rounded mb-0"
                            @if($solution->user_id != Auth::user()->id) disabled @endif >@lang('update')</button>
                            <a href="{{ route('solution.delete',$solution) }}"
                            onclick="event.preventDefault(); 
                                        document.getElementById('change-delete-form-{{ $solution->id }}').submit();"
                            class="btn btn-danger rounded mb-0" 
                            @if($solution->status == 'approved') disabled @endif >@lang('delete')</a>
                        @endif
                    </div>    
                </div>                       
                <hr class="my-0 py-1">
                <div class="form-group mx-0 px-0">
                    <label for="description">@lang('Describe your solution')</label>
                        <textarea name="description" id="description" 
                                class="form-control bg-light rounded shadow-sm 
                                        @error('description') is-invalid @else border-0 @enderror"
                                placeholder="Type the desription of the solution">{{ old('description', $solution->description)}}</textarea>
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
            <input type="hidden" name="bug_id" value="{{ $solution->bug_id }}">
        </form>
    </div>
    <form class="d-none"
          id="change-delete-form-{{ $solution->id }}"
          action="{{ route('solution.delete', $solution) }}" 
          method="post">
          @csrf @method('delete')
    </form>