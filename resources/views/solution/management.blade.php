@extends('layouts.app')

@section('title','Solution Management')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="bg-white rounded shadow px-4 py-3">
                    <div class="d-flex justify-content-between align-items-center mb-0">
                        <h1 class="display-5 mb-0">
                            Solution Management
                        </h1>
                        <a href="{{ route('solution.index') }}" class="btn btn-primary mb-0">
                            Solution List
                        </a>
                    </div>
                    <hr class="my-0 py-1">
                    <div class="col-12 my-0 py-0">
                        <h5 class="text-secondary d-inline font-weight-bold my-0 pb-1">Application: </h5>
                        <p class="text-secondary d-md-inline-block my-0 pb-1">{{ $solution->bug->getApplicationName() }}</p>
                    </div>
                    <div class="col-12 my-0 py-0">
                        <h5 class="text-secondary d-inline font-weight-bold my-0 pb-1">Bug: </h5>
                        <p class="text-secondary d-md-inline my-0 pb-1">{{ $solution->getBug()  }}</p>
                    </div>
                    <hr class="my-0 py-1">
                    <div class="col-12 my-0 py-1">
                        <table id="dinamic-steps" 
                            class="table table-sm table-striped table-bordered table-hover text-decoration-none" 
                            style="width:100%">
                            <thead class="bg-white">
                                <tr>
                                    <th>No.</th>
                                    <th>Steps</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($solution->bug->getBugsteps() as $step) 
                                    <tr>
                                        <td>{{ $step->order }}</td>
                                        <td>{{ $step->description }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr class="my-0 py-1">
                    <div class="col-12 mx-0 py-0">
                        <div class="d-flex justify-content-between align-items-center">
                                    <h4 class="text-secondary d-inline font-weight-bold mb-0">@lang('Solution')
                                        @if($solution->status != 'active')
                                            <small class="{{ $solution->status == 'approved' ? 'text-success' : 'text-danger' }}"> : {{ $solution->status }}</small>
                                        @endif
                                    </h4>
                                    <div class="button-group mb-0 pb-1">
                                        @if($solution->status == 'active')
                                            <a href="{{ route('solution.approved',$solution) }}"
                                            onclick="document.getElementById('approved').value = 'approved';
                                                     event.preventDefault(); 
                                                     document.getElementById('approved-form').submit();"
                                            class="btn btn-success rounded">@lang('Approved')</a>
                                            <form action="{{ route('solution.approved', $solution) }}" method="post"
                                                class="d-none" id="approved-form">
                                                @csrf @method('patch')
                                                <input type="hidden" name="approved" id="approved" value="">
                                            </form>
                                            <a href="{{ route('solution.rejected',$solution) }}"
                                            onclick="document.getElementById('rejected').value = 'rejected';
                                                     event.preventDefault(); 
                                                     document.getElementById('rejected-form').submit();"
                                            class="btn btn-danger rounded">@lang('Rejected')</a>
                                            <form action="{{ route('solution.rejected', $solution) }}" method="post"
                                                class="d-none" id="rejected-form">
                                                @csrf @method('patch')
                                                <input type="hidden" name="rejected" id="rejected" value="">
                                            </form>
                                        @endif
                                    </div>    
                                </div>                       
                                <hr class="my-0 py-1">
                                <div class="form-group mx-0 px-0">
                                    <label for="description pb-1">@lang('Describe your solution')</label>
                                        <textarea name="description" id="description" readonly="readonly"
                                                class="form-control bg-light rounded shadow-sm border-0">{{ $solution->description }}</textarea>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection