@extends('layouts.app')

@section('title','Application List')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            <h1 class="display-5 mb-0">@lang('Solutions List')</h1>
            <div class="btn-group">
                <a class="btn btn-success rounded mr-2" href="{{ route('bug.index') }}">@lang('Bug List')</a>
                @if(Auth::user()->isAdmin())
                    <a class="text-center pt-1" href="{{ route('solution.solutionExcelExport') }}"><i class="far fa-file-excel fa-2x"></i></a>
                @endif
            </div>
        </div>
        <hr>
        <table id="dinamic-application" 
               class="table table-sm table-bordered table-hover">
            <thead class="bg-white">
                <tr>
                    <th>Application</th>
                    <th>Bug</th>
                    <th>Solution</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Det</th>
                    <th class="text-center">Del</th>
                    @if(Auth::user()->isAdmin())
                        <th class="text-center">Check</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @forelse($solutions as $solution)
                    <tr>
                      <td>{{ $solution->bug->application->application }}</td>
                      <td>{{ $solution->bug->location }}</td>
                      <td>{{ $solution->description }}</td>
                      <td>{{ $solution->status }}</td>
                      <td class="text-center">
                            <a href="{{ route('bug.show', $bug = $solution->bug) }}" >
                                <i class="far fa-edit fa-2x text-primary"></i>
                            </a>
                      </td>
                      <td class="text-center">
                        @if($solution->status == 'active') 
                            <a href="{{ route('solution.delete',$solution) }}"
                            onclick="event.preventDefault(); 
                                        document.getElementById('change-delete-form-{{ $solution->id }}').submit();"> 
                                <i class="fas fa-trash-alt fa-2x text-danger"></i>
                            </a>
                            <form class="d-none"
                                id="change-delete-form-{{ $solution->id }}"
                                action="{{ route('solution.delete', $solution) }}" 
                                method="post">
                                @csrf @method('delete')
                            </form>
                        @endif  
                      </td>
                      @if($solution->solutionManagement())
                        <td class="text-center">
                            <a href="{{ route('solution.edit', $solution) }}">
                                <i class="fas fa-check-square fa-2x"></i>
                            </a>
                        </td>
                      @endif
                    </tr>
                @empty
                    <tr>
                      <td>@lang('No applications to dislay')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>

@endsection