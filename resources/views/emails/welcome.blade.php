<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome Mail</title>
</head>
<body style="background-color: #ecf0f1;">
    <table style="max-width: 600px;
                  padding: 10px;
                  margin: 0 auto;
                  border-collapse: collapse;">
        <tr>
            <td style="background-color: #ecf0f1; text-align: center; padding:0;">
                <h1>Hello {{ $user->name }} {{ $user->lastname }}</h1>
            </td>
        </tr>              
        <tr>
            <td style="background-color: white">
                <div style="background-color:34495e; margin: 4% 10% 2%; text-align:justify" >
                    <h5>Welcome to <strong>BUG TRACKER</strong></h5>
                    <p style="margin:2px; font-size:15px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, itaque tempore? Porro magnam natus et saepe quae suscipit nostrum impedit delectus ea fugit, unde sapiente deleniti! Magni, ut! Modi, aliquid.</p>
                    <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Regards,<br>
BugTracker</p>
                    <div style="width:100%; margin: 20px 0; 
                                display: inline-block; text-align:center ">
                        <a href="{{ url('/') }}"
                           style="text-decoration:none; border-radius: 5px;
                                  padding: 11px 23px; color: white; 
                                  background-color: #3498db;">
                            Bug Tracker Login
                        </a>
                    </div>
                </div> 
            </td>
        </tr>
    </table>
                
                
                

</body>
</html>