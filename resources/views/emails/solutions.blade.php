<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome Mail</title>
</head>
<body style="background-color: #ecf0f1;">
    <table style="max-width: 600px;
                  padding: 10px;
                  margin: 0 auto;
                  border-collapse: collapse;">
        <tr style="border-bottom: 1px solid #e8e5ef;">
            <td style="background-color: white; border-spacing:1px; text-align: center; padding: 6px">
                <img src="https://i.postimg.cc/90BgtLrn/btlogoheader.png" alt="BugTracker">
            </td>
        </tr>
        <tr>
            <td style="background-color: white; text-align: center; padding:0;">
                <h3>Congratulations</h3>
            </td>
        </tr>  
        <tr>
            <td style="background-color: white; text-align: center; margin: 0 auto; padding:0;">
                <h5>We have recieved your solution</h5>
            </td>
        </tr>            
        <tr>
            <td style="background-color: white">
                <div style="background-color:34495e; margin: 4% 10% 2%; text-align:justify" >
                    <h5>Hi {{ $user->name }} {{ $user->lastname }}</h1>
                    <p style="margin:2px; font-size:15px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, itaque tempore? Porro magnam natus et saepe quae suscipit nostrum impedit delectus ea fugit, unde sapiente deleniti! Magni, ut! Modi, aliquid.</p>
                    <br>
                    <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Regards,
                    <br>
                        BugTracker
                    </p>
                    <div style="width:100%; margin: 20px 0; 
                                display: inline-block; text-align:center ">
                        <a href="{{ url('/') }}"
                           style="text-decoration:none; border-radius: 5px;
                                  padding: 11px 23px; color: white; 
                                  background-color: #3498db;">
                            Bug Tracker Login
                        </a>
                    </div>
                </div> 
            </td>
        </tr>
        <tr>
            <td style="background-color: white">
                <table style="position: relative; border-top: 1px solid #e8e5ef;
                              margin: 0 auto; padding: 0; text-align: center; width: 570px;">
                    <tr>
                        <td style="position: relative; max-width: 100vw; padding: 32px;">
                            <p style="position: relative; line-height: 1.5em; 
                                      margin-top: 0; font-size: 12px; 
                                      text-align: justify;">
                               If you’re having trouble clicking the "Bug Tracker Login" button, copy and paste the URL below into your web browser: BugTracker.test
                            </p>
                    </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="570" cellpadding="0" cellspacing="0" role="presentation" 
                       style="position: relative;
                              margin: 0 auto; padding: 0; text-align: center; width: 570px;">
                    <tr>
                        <td style="position: relative; max-width: 100vw; padding: 32px;">
                            <p style="position: relative; line-height: 1.5em; 
                                      margin-top: 0; color: #b0adc5; font-size: 12px; 
                                      text-align: center;">
                                © 2020 BugTracker. All rights reserved.
                            </p>
                    </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                
                
                

</body>
</html>