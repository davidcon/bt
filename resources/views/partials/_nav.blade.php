 <div class="sticky-top">
    <nav class="navbar navbar-light navbar-expand-lg bg-light shadow">
        <div class="container">
        @guest
            <a class="navbar-brand" href="{{ url('/') }}">
            <h1 class="display-5 text-danger text-shadow">{{ config('app.name', 'Laravel') }}</h1>  
            </a>
        @else    
            <button class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    arial-controls="navbarSupportedContent"
                    arial-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">   
                <span class="navbar-toggler-icon"></span>
            </button>
            <a href="{{ route('home') }}" class="navbar-brand">
                <img src="/img/btlogoheader.svg" width="160" height="40" alt="BugTracker" class="img-fluid ">
            </a>    
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                        <a href="{{ route('home') }}" class="nav-link h5">Dashboard</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle h5" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Application
                        </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @if(Auth::user()->isAdmin())
                                <a class="dropdown-item" href="{{ route('application.create') }}">Create</a>
                            @endif
                            <a class="dropdown-item" href="{{ route('application.index') }}">List</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle h5" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Bug
                        </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('bug.create') }}">Create</a>
                            <a class="dropdown-item" href="{{ route('bug.index') }}">List</a>
                        </div>
                    </li>
                    @if(Auth::user()->isAdmin())
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle h5" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Severity
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('severity.create') }}">Create</a>
                                <a class="dropdown-item" href="{{ route('severity.index') }}">List</a>
                            </div>
                        </li>
                    @endif
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle h5" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Solution
                        </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('solution.index') }}">List</a>
                        </div>
                    </li>    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle h5" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span>
                                @if(Auth::user()->role->role == "Admin")
                                    <i class="fas fa-user-tie"></i> 
                                @else
                                    <i class="fas fa-user"></i> 
                                @endif
                            </span>
                                {{ auth()->user()->userFullName() }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('user.edit', ['user'=> Auth::user()] ) }}">Profile</a>
                            <a class="dropdown-item" href="{{ route('solution.solutionExcelExport', ['user'=> Auth::user()] ) }}">My Score Board</a>
                            @if(Auth::user()->isAdmin())
                                <a class="dropdown-item" href="{{ route('position.index') }}">Position</a>
                                <a class="dropdown-item" href="{{ route('role.index') }}">Role</a>
                                <a class="dropdown-item" href="{{ route('user.create') }}">New User</a>
                                <a class="dropdown-item" href="{{ route('user.index') }}">List User</a>
                            @endif
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link h5" href="{{ route('logout') }}" 
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit()">
                            <span>
                                <i class="fas fa-sign-out-alt"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        @endguest
        </div>
    </nav>
    <form id="logout-form" action="{{ route('logout') }}" method="post" class="d-none">@csrf</form>
</div>