<div class="px-2 py-2">
    <form action="{{ route('position.store') }}" method="post"
        class="bg-white rounded shadow-sm px-3 py-2">
        @csrf
        <div class="form-row mb-0">
            <div class="form-group col-sm-12 col-md-2 mb-0">
                <h5 class="display-5 mt-2 pl-4">@lang('New position')</h5>
            </div>
            <div class="form-group col-sm-12 col-md-8 mb-0">
                <input name="position"
                    placehoder="Type the position name"
                    value="{{ old('position') }}" 
                    class="form-control bg-light shadow-sm @error('position') is-invalid @else border-0 @enderror">
                @error('position')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form-group col-sm-12 col-md-2 mb-0">
                <button type="submit" class="btn btn-success">@lang('Save Position')</button>
            </div>
        </div>       
    </form>
</div>