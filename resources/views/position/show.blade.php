<div class="bg-white px-4 py-2">
    <table id="dinamic-bugs" 
        class="table table-sm table-bordered table-hover" 
        style="width:100%">
        <thead class="bg-light">
            <tr>
                <th>Position</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach($positions as $position) 
                <tr>
                    <td>
                        {{ $position->position }}
                    </td>
                    <td class="text-center">
                        <a href="{{ route('position.edit', $position) }}" class="col-sm-2" >
                            <i class="far fa-edit fa-2x text-primary"></i>
                        </a>  
                    </td>
                    <td class="text-center">
                        @if($position->users->count()>0)
                            <i class="fas fa-ban fa-2x text-danger"></i>
                        @else
                            <a href="{{ route('position.delete', $position) }}"
                            onclick="event.preventDefault(); 
                                        document.getElementById('change-delete-form-{{ $position->id }}').submit();">
                                <i class="fas fa-trash-alt fa-2x text-dark"></i>
                            </a>
                        @endif                         
                           <form class="d-none"
                                id="change-delete-form-{{ $position->id }}"
                                action="{{ route('position.delete', $position) }}" 
                                method="post">
                                @csrf @method('delete')
                            </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>