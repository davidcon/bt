@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row bg-white my-3 mx-auto rounded shadow">
        <div class="col-12 col-sm-10 col-lg-6 bg-white mx-auto">
            <img src="/img/logobugtracker.svg" alt="BugTracker" class="img-fluid mx-auto my-2 d-block">
        </div>
        <div class="col-12 col-sm-10 col-lg-6 mx-auto">
            <form action="{{ route('login') }}" 
                  method="post"
                  class="bg-light rounded shadow my-5 mx-auto py-3 px-4">
                @csrf
                <h1 class="display-5 text-center">@lang('BugTracker Login')</h1>
                <hr>
                <div class="form-group">
                    <label for="email" 
                           class="col-sm-10 col-md-12 col-form-label">
                           {{ __('E-Mail') }}
                    </label>
                    <div class="col-sm-10 col-md-12">
                        <input id="email" type="email"
                               placeholder="Type your e-mail..." 
                               class="form-control rounded shadow-sm 
                                      @error('email') is-invalid @else border-0 @enderror" 
                               name="email" value="{{ old('email') }}" 
                               required
                        >
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" 
                           class="col-sm-10 col-md-12 col-form-label">
                           {{ __('Password') }}
                    </label>
                    <div class="col-sm-10 col-md-12">
                        <input id="password" type="password" 
                               placeholder="Type your password..."
                               class="form-control rounded shadow-sm
                                      @error('password') is-invalid @else border-0 @enderror" 
                                name="password" required autocomplete="current-password"
                        >
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                        <div class="col-sm-12 col-md-6 px-5 my-0">
                            <input class="form-check-input" 
                                   type="checkbox" name="remember" 
                                   id="remember" {{ old('remember') ? 'checked' : '' }}
                        >
                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                        </div>
                        <div class="col-sm-12 col-md-6 mx-auto my-0">
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                        </div>

                        
                </div>

                <div class="form-group">
                    <div class="col-12">
                        <button type="submit" class="btn btn-large btn-primary btn-block">
                            {{ __('Login') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
   
@endsection
