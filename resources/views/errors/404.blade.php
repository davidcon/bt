@extends('layouts.errors')

@section('title','Unauthorized')

@section('content')
    <div class="container h.screen">
        <div class="row bg-white mx-auto px-5 rounded shadow">
            <div class="col-12 col-sm-10 col-lg-6 bg-white mx-auto">
                <img src="/img/logobugtracker.svg" class="img-fluid mx-auto my-2 d-block">
            </div>
            <div class="col-12 col-sm-10 col-lg-6 mx-auto py-5">
                <h1 class="display-5 text-danger text-center text-shadow mt-4">Page Not Found</h1>
                <hr>
                <p class="text-secondary text-justify pb-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi sapiente laborum est, maiores temporibus deserunt in tenetur nostrum quibusdam exercitationem repellat illum officia voluptates saepe corrupti natus incidunt sed aut.</p>
                <a href="{{ url()->previous() }}" class="btn btn-dark btn-block">Back to App</a>
            </div>
        </div>
    </div>
@endsection