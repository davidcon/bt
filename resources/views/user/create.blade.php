@extends('layouts.app')

@section('title','New User')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form method="POST" 
                      action="{{ route('user.store') }}"
                      class="bg-white rounded shadow mx-auto my-3 px-4 py-3">
                    @csrf
                    <div class="d-flex justify-content-between align-items-center">
                        <h1 class="display-5 pl-3">@lang('New User')</h1>
                        
                            <a href="{{ route('user.index') }}" class="btn btn-primary">@lang('User List')</a>
                        
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-sm-10 col-md-6">
                            <label for="name">{{ __('Name') }}</label>
                                <input id="name" type="text"
                                       name="name" value="{{ old('name') }}" 
                                       required autocomplete="name"
                                       placeholder="Type a name..."
                                       class="form-control shadow-sm 
                                              @error('name') is-invalid @else border-0 @enderror"  autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group col-sm-10 col-md-6">
                            <label for="lastname">{{ __('Lastname') }}</label>
                                <input id="lastname" type="text"
                                       name="lastname" value="{{ old('lastname') }}" 
                                       required autocomplete="lastname"
                                       placeholder="Type a lastname..." 
                                       class="form-control shadow-sm 
                                              @error('lastname') is-invalid @else border-0 @enderror" 
                                       autofocus>
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-3">
                            <label for="role_id">@lang('Role')</label>
                            <select name="role_id" required
                                    class="form-control shadow-sm 
                                           @error('role_id') is-invalid @else border-0 @enderror">
                                <option value="" selected>Choose an role...</option>
                                @forelse($roles as $role )
                                    <option 
                                           value="{{ $role->id }}" 
                                           @if($role->id == old('role_id')) selected @endif>{{ $role->role }}</option>
                                @empty
                                    <option>There is no role list</option>
                                @endforelse
                            </select>
                            @error('role_id')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                        </div>
                    <div class="form-group col-md-3">
                            <label for="position_id">@lang('Position')</label>
                            <select name="position_id" required
                                    class="form-control shadow-sm 
                                           @error('position_id') is-invalid @else border-0 @enderror">
                                <option value="" selected>Choose an position...</option>
                                @forelse($positions as $position )
                                    <option 
                                           value="{{ $position->id }}" 
                                           @if($position->id == old('position_id')) selected @endif>{{ $position->position }}</option>
                                @empty
                                    <option>There is no position list</option>
                                @endforelse
                            </select>
                            @error('position_id')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" 
                                       class="form-control shadow-sm
                                              @error('email') is-invalid @else border-0 @enderror" 
                                              name="email" value="{{ old('email') }}" 
                                              required autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>    
                    <div class="form-row">
                        <div class="form-group col-sm-10 col-md-6">
                            <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" 
                                       class="form-control shadow-sm
                                              @error('password') is-invalid @else border-0 @enderror" 
                                       name="password" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group col-sm-10 col-md-6">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" 
                                       class="form-control border-0 shadow-sm" name="password_confirmation" 
                                       required autocomplete="new-password">
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-success">
                            {{ __('Save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection