@extends('layouts.app')

@section('title','New User')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form method="post" 
                      action="{{ route('user.update', $user) }}"
                      class="bg-white rounded shadow mx-auto my-3 px-4 py-3">
                    @csrf @method('patch')
                    <div class="d-flex justify-content-between align-items-center">
                        <h1 class="display-5 pl-3"> 
                            <span>
                                @if($user->isAdmin())
                                    <i class="fas fa-user-tie text-secondary"></i> 
                                @else
                                    <i class="fas fa-user text-primary"></i> 
                                @endif
                            </span>
                            @lang('User Profile')
                            <strong class="pl-3">Score : </strong>
                            <i class="fas fa-trophy text-warning">{{ $user->userScore() }}</i>     
                        </h1>
                        @if(Auth::user()->role->role == 'Admin')
                            <a href="{{ route('user.index') }}" class="btn btn-primary">@lang('User List')</a>
                        @endif
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-sm-10 col-md-6">
                            <label for="name">{{ __('Name') }}</label>
                                <input id="name" type="text"
                                       name="name" 
                                       value="{{ old('name', $user->name) }}" 
                                       required autocomplete="name"
                                       placeholder="Type a name..."
                                       class="form-control shadow-sm 
                                              @error('name') is-invalid @else border-0 @enderror" >
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group col-sm-10 col-md-6">
                            <label for="lastname">{{ __('Lastname') }}</label>
                                <input id="lastname" type="text"
                                       name="lastname" value="{{ old('lastname',$user->lastname) }}" 
                                       required autocomplete="lastname"
                                       placeholder="Type a lastname..." 
                                       class="form-control shadow-sm 
                                              @error('lastname') is-invalid @else border-0 @enderror" >
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-3">
                            <label for="role_id">@lang('Role')</label>                        
                                <select name="role_id"
                                        class="form-control shadow-sm 
                                            @error('role_id') is-invalid @else border-0 @enderror">
                                    @if($user->role->role != 'Admin' && Auth::user()->role->role != 'Admin')
                                        <option value="{{ $user->role_id }}" selected>{{ $user->role->role }}</option>
                                    @else    
                                    <option value="" selected>Choose an role...</option>
                                        @forelse($roles as $role )
                                            <option 
                                                value="{{ $role->id }}" 
                                                @if($role->id == old('role_id',$user->role_id)) selected @endif>{{ $role->role }}</option>
                                        @empty
                                            <option>There is no role list</option>
                                        @endforelse
                                    @endif    
                                </select>
                            @error('role_id')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="position_id">@lang('Position')</label>
                            <select name="position_id"
                                    class="form-control shadow-sm 
                                           @error('position_id') is-invalid @else border-0 @enderror">
                                @if($user->role->role != 'Admin' && Auth::user()->role->role != 'Admin')
                                    <option value="{{ $user->position_id }}" selected>{{ $user->position->position }}</option>
                                @else  
                                    <option value="" selected>Choose an position...</option>
                                    @forelse($positions as $position )
                                        <option 
                                            value="{{ $position->id }}" 
                                            @if($position->id == old('position_id',$user->position_id)) selected @endif>{{ $position->position }}</option>
                                    @empty
                                        <option>There is no position list</option>
                                    @endforelse
                                @endif
                            </select>
                            @error('position_id')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" readonly="readonly"
                                       class="form-control shadow-sm
                                              @error('email') is-invalid @else border-0 @enderror" 
                                              name="email" value="{{ old('email',$user->email) }}" 
                                              >
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="button-group mb-0">
                        <button type="submit" class="btn btn-success">
                            {{ __('Update') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection