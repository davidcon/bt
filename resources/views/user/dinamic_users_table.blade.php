<div class="col-12 bg-light rounded py-1 my-2">
    <table id="dinamic-users" 
           class="table table-sm table-striped table-bordered table-hover" 
           style="width:100%">
        <thead class="bg-white">
            <tr>
                <th>Name</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>Role</th>
                <th>Poition</th>
                <th>Profile</th>
                <th>Status</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user) 
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->role }}</td>
                    <td>{{ $user->position->position }}</td>
                    <td class="text-center">
                        <a href="{{ route('user.edit',$user) }}" class="text-decoration-none">
                            <span>
                                @if($user->role->role == "Admin")
                                    <i class="fas fa-user-tie fa-2x"></i> 
                                @else
                                    <i class="fas fa-user  fa-2x"></i> 
                                @endif
                            </span>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="{{ route('user.manage', $user) }}"
                           onclick="event.preventDefault(); 
                            document.getElementById('change-status-form-{{ $user->id }}').submit();">
                           @if( $user->status == 'active')   
                                <i class="fas fa-power-off fa-2x text-success"></i>
                            @else
                                <i class="fas fa-power-off fa-2x text-danger"></i>
                           @endif   
                        </a>
                        <form   class="d-none" 
                                id="change-status-form-{{ $user->id }}"
                                action="{{ route('user.manage', $user) }}" 
                                method="post">
                                @csrf @method('patch')
                                <input type="hidden" name="status" value=" @if($user->status == 'active') inactive @else active @endif ">
                        </form>
                    </td>
                    <td class="text-center">
                    @if($user->isAllowDelete($user->id))
                        <a href="{{ route('user.delete', $user) }}"
                            onclick="event.preventDefault(); 
                                     document.getElementById('change-delete-form-{{ $user->id }}').submit();">
                            <i class="fas fa-trash-alt fa-2x text-dark"></i>
                        </a>                      
                        <form class="d-none"
                              id="change-delete-form-{{ $user->id }}"
                              action="{{ route('user.delete', $user) }}" 
                              method="post">
                              @csrf @method('delete')
                        </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>