@extends('layouts.app')

@section('title','User List')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="display-5 mb-0">@lang('User List')</h1>
                    <div class="btn-group">
                        <a href="{{ route('user.create') }}" class="btn btn-success rounded mr-2">@lang('New User')</a>
                        <a href="{{ route('user.userExcelExport') }}" class="pt-1"><i class="far fa-file-excel fa-2x"></i></a>
                    </div>
                    
                </div>
                <hr>
                @include('user.dinamic_users_table')
            </div>
        </div>
    </div>
@endsection