<div class="bg-light px-4 py-2">
    <table id="dinamic-bugs" 
        class="table table-sm table-bordered table-hover" 
        style="width:100%">
        <thead class="bg-light">
            <tr>
                <th>Role</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach($roles as $role) 
                <tr>
                    <td>
                        {{ $role->role }}
                    </td>
                    <td class="text-center">
                        <a href="{{ route('role.edit', $role) }}" class="col-sm-2" >
                            <i class="far fa-edit fa-2x text-primary"></i>
                        </a>  
                    </td>
                    <td class="text-center">
                        @if($role->users->count()>0)
                            <i class="fas fa-ban fa-2x text-danger"></i>
                        @else
                            <a href="{{ route('role.delete', $role) }}"
                            onclick="event.preventDefault(); 
                                        document.getElementById('change-delete-form-{{ $role->id }}').submit();">
                                <i class="fas fa-trash-alt fa-2x text-dark"></i>
                            </a>
                        @endif                         
                           <form class="d-none"
                                id="change-delete-form-{{ $role->id }}"
                                action="{{ route('role.delete', $role) }}" 
                                method="post">
                                @csrf @method('delete')
                            </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>