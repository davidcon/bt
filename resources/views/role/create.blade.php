<div class="px-2 py-2">
    <form action="{{ route('role.store') }}" method="post"
        class="bg-white rounded shadow px-3 py-2">
        @csrf
        <div class="form-row mb-0">
            <div class="form-group col-sm-12 col-md-2 mb-0">
                <h5 class="display-5 mt-2 pl-4">@lang('New Role')</h5>
            </div>
            <div class="form-group col-sm-12 col-md-8 mb-0">
                <input name="role"
                    placehoder="Type the role name"
                    value="{{ old('role') }}" 
                    class="form-control bg-light shadow-sm @error('role') is-invalid @else border-0 @enderror">
                @error('role')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form-group col-sm-12 col-md-2 mb-0">
                <button type="submit" class="btn btn-success">@lang('Save Role')</button>
            </div>
        </div>       
    </form>
</div>