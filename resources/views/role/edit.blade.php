@extends('layouts.app')

@section('title','Position List')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="display-5 mb-0">@lang('Edit Role')</h1>
                </div>
                <hr>
                <div class="px-2 py-2">
                    <form action="{{ route('role.update',$role) }}" method="post"
                        class="bg-white rounded shadow-sm px-3 py-2">
                        @csrf @method('patch')
                        <div class="form-row mb-0">
                            <div class="form-group col-sm-12 col-md-2 mb-0">
                                <h5 class="display-5 mt-2 pl-4">@lang('New role')</h5>
                            </div>
                            <div class="form-group col-sm-12 col-md-8 mb-0">
                                <input name="role"
                                    placehoder="Type the role name"
                                    value="{{ old('role',$role->role) }}" 
                                    class="form-control bg-light shadow-sm @error('role') is-invalid @else border-0 @enderror">
                                @error('role')
                                    <span class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-sm-12 col-md-2 mb-0">
                                <button type="submit" class="btn btn-success ">@lang('Update Role')</button>
                            </div>
                        </div>       
                    </form>
                </div>
                <hr>
                @include('role.show')
            </div>
        </div>
    </div>
@endsection