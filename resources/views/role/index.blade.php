@extends('layouts.app')

@section('title','Position List')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="display-5 mb-0">@lang('Position List')</h1>
                </div>
                <hr>
                @include('role.create')
                <hr>
                @include('role.show')
            </div>
        </div>
    </div>
@endsection