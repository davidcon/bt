<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Role; 

class RoleTest extends TestCase
{
     use RefreshDatabase;
     
    /**
     * @testdox Create a new Role
     * @test
     */

    //  public function testAddRole(){
    //      $this->assertEquals(0, Role::count());
    //      factory(Role::class)->create();
    //      $this->assertEquals(1, Role::count());
    //  }

    /**
     * @testdox add a Role addRoleTest
     * @test 
     */
    public function addRoleTest(){

        //$this->withoutExceptionHandling();

        $response = $this->post('/role',[
            'role'=>'admin',
        ]);

        $role = Role::first();
        //$response->assertOk();

        $this->assertEquals(1,Role::count());

        $response->assertRedirect('role');
    }

    /**
     * @testdox field role is required
     * @test
     */
    public function roleFieldIsRequired(){

        //$this->withoutExceptionHandling();

        $response = $this->post('/role',[
            'role'=>'admin',
        ]);
    
        $response->assertSessionHasNoErrors('/role');
        
    }

    /**
     * @testdox Updating a Role
     * @test
     */
    public function updateRoleTest(){

        //$this->withoutExceptionHandling();
        
        $this->post('/role',[
            'role'=>'admn',
        ]);

        $role = Role::first();
        
        $response = $this->patch($role->path(),[
            'role'=>'Admin',
        ]);

        $response->assertSessionHasNoErrors('role');

        $this->assertEquals('Admin', Role::first()->role);

        $response->assertRedirect($role->path());
    }

    /**
     * @testdox delete a role
     * @test
     */
    public function deleteRoleTest(){

        $this->withoutExceptionHandling();

        $this->post('/role',[
            'role'=>'admn',
        ]);
        
        $this->assertCount(1,Role::all());

        $role = Role::first();

        $response = $this->delete($role->path());

        $this->assertCount(0, Role::all());

        $response->assertRedirect('/role');
    }
}
 