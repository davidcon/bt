<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Position;
use Carbon\Carbon;

class PositionTest extends TestCase
{
    use RefreshDatabase;

    public function data(){
        return [
            'position'=>'Supervisor',
        ];
    }

    /**
     * @testdoc any validation field are going to be tested
     * @test 
     */
    public function positionFieldsValidation(){

        $this->withoutExceptionHandling();

        $response = $this->post('/position', $this->data());

        $position = Position::all();

        $this->assertCount(1,$position);
        $this->assertInstanceOf(Carbon::class, $position->first()->created_at);
        $this->assertInstanceOf(Position::class, $position->first());

        $response->assertSessionHasNoErrors('position');
    }
    /**
     * @testdox add a new position
     * @test
     */
    public function addPositionTest(){

        $this->withoutExceptionHandling();

        $response = $this->post('/position',$this->data());

        $this->assertCount(1, Position::all());
    }

    /**
     * @testdox update a position
     * @test
     */
    public function updateRoleTest(){

        $this->withoutExceptionHandling();

        $this->post('/position',$this->data());

        $this->assertCount(1, Position::all());

        $position = Position::first();

        $response = $this->patch($position->path(),$this->data());

        $this->assertEquals('Supervisor', $position->fresh()->position);

        $response->assertRedirect($position->path());
    }

    /**
     * @testdox delete a position
     * @test
     */
    public function deletePositionTest(){

        $this->withoutExceptionHandling();

        $this->post('/position',$this->data());

        $this->assertCount(1, Position::all());

        $position = Position::first();

        $response = $this->delete($position->path());

        $this->assertCount(0, Position::all());
    }

}
