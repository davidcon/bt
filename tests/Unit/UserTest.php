<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\User;
use App\Position;
use App\Role;


class UserTest extends TestCase
{
    use RefreshDatabase;

    public function positionData(){
        return [
                   'position'=>'New position',
               ];
    }

    public function getPositionId(){
        $this->post('/position', $this->positionData());
        $position = Position::first();
        return $position->id;
    }

    public function roleData(){
        return [
                   'role'=>'new role',
               ];
    }

    public function getRoleId(){
        $this->post('/role', $this->roleData());
        $role = Role::first();
        return $role->id;
    }

    public function userData(){

        return [
                'role_id'=>$this->getRoleId(),
                'position_id'=>$this->getPositionId(),
                'name'=>'David',
                'lastname'=>'Consuegra',
                'email'=>'david.consuegra@bugtracker.com',
                'password'=>'password',
                'password_confirmation'=>'password',
        ];
    }
    
    /**
     * @testdox Create a user
     * @test
     */
    public function createUserTest(){

        $this->withoutExceptionHandling();

        $response = $this->post('/register',$this->userData());

        $user = User::all();

        $this->assertCount(1, $user);

        $this->assertEquals('David', $user->first()->name);
        $this->assertEquals(1, $user->first()->role->id);
        $this->assertEquals('new role', $user->first()->role->role);

        $response->assertSessionHasNoErrors(['role_id','position_id','name']);
        
    }
}