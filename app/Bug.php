<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Bug extends Model
{
    protected $fillable = [
        'user_id','application_id','severity_id','location','description',
    ];

    protected $hidden = [
        'status',
    ];

    public function application()
    {
        return $this->belongsTo('App\Application');
    }

    public function severity()
    {
        return $this->belongsTo('App\Severity');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function steps()
    {
        return $this->hasMany('App\Step');
    }

    public function solutions()
    {
        return $this->hasMany('App\Solution');
    }

    public function getApplicationName()
    {
        return $this->application->application;
    }

    public function getBugSteps()
    {
        return $this->steps;
    }

    public function getBugStepsQuantity()
    {
        if($this->steps->count() > 0)
        {
            return $this->steps->count();
        }
        return 0;
    }

    public function getSeverity()
    {
        return $this->severity->severity;
    }

    public function getOwnerFullname()
    {
        return $this->user->name.' '.$this->user->lastname;
    }

    public function getOwner()
    {
        return $this->user_id;
    }

    public function allowEdit()
    {
        if((Auth::user()->isAdmin() || $this->getOwner() === Auth::user()->id) && $this->status == 'active')
        {
            return true;
        }
        return false;
    }

    public function allowDelete()
    {
        if((Auth::user()->isAdmin() || $this->getOwner() === Auth::user()->id) && $this->status == 'active'){
            return true;
        }
        return false;
    }
}
