<?php

namespace App\Providers;

use App\Policies\PostPolicy;
use App\Post;
use App\Policies\BugPolicy;
use App\Bug;
use App\Policies\UserPolicy;
use App\User;
use App\Policies\RolePolicy;
use App\Role;
use App\Policies\PositionPolicy;
use App\Position;
use App\Policies\SolutionPolicy;
use App\Solution;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Application::class => ApplicationPolicy::class,
        Bug::class => BugPolicy::class,
        User::class => UserPolicy::class,
        Role::class => RolePolicy::class,
        Position::class => PositionPolicy::class,
        Solution::class => SolutionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
    }
}
