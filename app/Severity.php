<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Severity extends Model
{
    protected $fillable = [
        'severity','description','score','status',
    ];

    public function bugs(){
        return $this->hasMany('App\Bug');
    }
}
