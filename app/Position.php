<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = [
        'position'
    ];

    public function users(){
        return $this->hasMany('App\User');
    }

    public function path(){
        return '/position/'.$this->id;
    }
}
