<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SolutionMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $solutionData;

    public function __construct($user, $solutionData)
    {
        $this->user = $user;
        $this->solutionData = $solutionData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.solutions');
    }
}
