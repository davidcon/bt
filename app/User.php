<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use App\Bug;
use App\Solution;

class User extends Authenticatable 
{
    use Notifiable;

    protected $fillable = [
        'role_id','position_id','name', 'lastname','email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token','status',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function position(){
        return $this->belongsTo('App\Position');
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function bugs(){
        return $this->hasMany('App\Bug');
    }

    public function solutions(){
        return $this->hasMany('App\Solution');
    }

    public function userFullName(){
        return $this->name.' '.$this->lastname;
    }

    public function userScore(){
        if($this->solutions->count() > 0){
            return Solution::select('score')
                             ->where([
                                       ['status','approved'],
                                       ['user_id',$this->id]
                                    ])
                             ->get()
                             ->sum('score'); 
        }
        return 0;
    }

    public function getRole()
    {
        return $this->role->role;
    }

    public function getPosition()
    {
        return $this->position->position;
    }

    public function hasBugs($userId){
        if(Bug::where('user_id','=',$userId)->count() > 0){
            return true;
        }
        return false;
    }

    public function hasSolutions($userId){
        if(Solution::where('user_id','=',$userId)->count() > 0){
            return true;
        }
        return false;
    }

    public function isAdmin(){
        if($this->getRole() == 'Admin'){
            return true;
        }
        return false;
    }

    public function isAllowEdit($user){
        if(Auth::user()->isAdmin() || ($user->id === Auth::user()->id)){
            return true;
        }
        return false;
    }

    public function isAllowDelete($user){
        if( Auth::user()->isAdmin() 
            && ($this->hasBugs($user) == false )
            && ($this->hasSolutions($user) === false)){
            return true;
        }
        return false;
    }

    public function path(){
        return '/user/'.$this->id;
    }
}
