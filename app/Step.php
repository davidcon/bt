<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $fillable = [
        'bug_id','order','description'
    ];

    public function bug(){
        return $this->belongsTo('App\Bug');
    }
}
