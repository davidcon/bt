<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Application;
use App\Exports\ApplicationExport;


class ApplicationController extends Controller
{

    public function index(Request $request)
    {
        $applications = Application::all();
        return view('application.index', compact('applications'));
    }

    public function applicationExcelExport()
    {
        $this->authorize('applicationExcelExport', Application::class);

        $applicationExport = new ApplicationExport('');
        return $applicationExport->download('Application.xlsx');
    }

    public function create(Request $request, Application $application)
    {
        $this->authorize('create', Application::class);
        return view('application.create');
    }

    public function store(Request $request, Application $application )
    {
        $this->authorize('create', Application::class);

        $applicationData = $request->validate([
            'application'=>'required|min:5|max:255',
            'description'=>'required|min:10',
        ]);

        $application->create($applicationData);

        flash('Application has been created')->success();
        return redirect()->route('application.index');
    }

    public function show(Application $application)
    {   
        return view('application.show',compact('application'));
    }

    public function edit(Request $request, Application $application)
    {
        $this->authorize('update',$application);
        return view('application.edit',compact('application'));
    }

    public function update(Request $request, Application $application)
    {   
        $this->authorize('update', $application);

        $applicationData = $request->validate([
            'application'=>'required|min:5|max:255',
            'description'=>'required|min:10',
        ]);

        $application->update($applicationData);

        flash('Application has been updated')->success();
        return redirect()->route('application.edit', compact('application'));
    }

    public function manageApplicationStatus(Request $request, Application $application)
    {
        $this->authorize('update', $application);

        $application->status = $request->status;
        $application->save();

        flash('Application has been '.$request->status)->success();
        return redirect()->route('application.index', compact('application'));
    }

    public function destroy(Application $application)
    {
        $this->authorize('delete', $application);
        
        $application->delete();

        flash('Application has been deleted')->success();
        return redirect()->route('application.index');
    }
}
