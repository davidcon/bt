<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Severity;

class SeverityController extends Controller
{

    public function index()
    {
        $severities = Severity::paginate(5);
        return view('severity.index',compact('severities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('severity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Severity $severity)
    {
        $severityData = $request->validate([
            'severity'=>'required|min:3|max:255',
            'score'=>'required|numeric|integer|between:0,100',
            'description'=>'required',
            'status'=>'filled',
        ]);

        $severity = Severity::create($severityData);
        flash('Severity has been created')->success();
        return redirect()->route('severity.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Severity $severity)
    {
        return view('severity.edit', compact('severity'));
    }

    public function update(Request $request, Severity $severity)
    {
        $severityData = $request->validate([
            'severity'=>'filled|required|min:3|max:255',
            'score'=>'filled|required|numeric|integer|between:0,100',
            'description'=>'filled|required',
            'status'=>'filled',
        ]);

        $severity->update($severityData);
        flash('Severity has been updated');
        return redirect()->route('severity.index');
    }

    public function destroy(Severity $severity)
    {
        $severity->delete();
        flash('Severity has been deleted')->success();
        return redirect()->route('severity.index');
    }
}
