<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Step;

class StepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $step = Step::select('order')->where('bug_id','=',$bug->id)->count();
        //dd($step);
    }

    public function create(Step $step)
    {
        return view('step.create',compact('step'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Step $step)
    {
        $stepData = $request->validate([
            'description'=>'required|min:10|max:255',
            'bug_id'=>'required',
        ]);
        
        $order = Step::select('order')
                       ->where('bug_id','=',$request->bug_id)->count()+1;

        $step = Step::create([
            'bug_id'=>$request->bug_id,
            'description'=>$request->description,
            'order'=>$order,
        ]);
        
        flash('Step has been created')->success();
        return view('step.create',compact('step'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
