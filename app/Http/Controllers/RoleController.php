<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Role;

class RoleController extends Controller
{

    public function index()
    {
        $this->authorize('viewAny', Role::class);
        $roles = Role::all();
        return view('role.index',compact('roles'));
    }

    public function store(Request $request)
    {
        $this->authorize('create', Role::class);

        $roleData = $request->validate([
            'role'=>'required|min:5'
        ]);

        Role::create($roleData);

        flash('Role has been created')->success();
        return redirect()->route('role.index');
    }

    public function edit(Role $role)
    {
        $this->authorize('update', $role);

        $roles = Role::all();
        return view('role.edit',compact('roles','role'));
    }

    public function update(Request $request, Role $role)
    {
        $this->authorize('update', $role);

        $roleData = $request->validate([
            'role'=>'required|min:5',
        ]);

        $role->update($roleData);
        flash('Role has been updated')->success();
        return redirect()->route('role.index');
    }

    public function destroy(Role $role)
    {
        $this->authorize('delete', $role);

        $role->delete();
        
        flash('Role has been deleted')->success();
        return redirect()->route('role.index');
    }
}
