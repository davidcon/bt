<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use App\Exports\BugExport;
use App\Bug;
use App\Application;
use App\Severity;
use App\Step;
use App\Solution;

class BugController extends Controller
{
    public function index(Request $request)
    {
        $bugs = Bug::whereIn('status',['active','pending'])->get();

        return view('bug.index',compact('bugs'));
    }

    public function bugExcelExport()
    {
        $this->authorize('bugExcelExport', Bug::class);

        $bugExport = new BugExport('');
        return $bugExport->download('Bugs.xlsx');
    }

    public function create()
    {
        $applications = Application::select('id','application')
                        ->where('status','active')
                        ->orderBy('application','asc')
                        ->get();

        $severities = Severity::select('id','severity')
                      ->where('status','active')
                      ->orderBy('severity','asc')
                      ->get();

        return view('bug.create',compact('applications','severities'));
    }

    public function store(Request $request, Bug $bug)
    {
        $request->validate([
            'application_id'=>'required',
            'severity_id'=>'required',
            'location'=>'required|min:10|max:255',
            'description'=>'required|min:10',
            'step_description'=>'required|min:10',
        ]);

        DB::beginTransaction();

        try {
            $newBug = Bug::create([   
                                    'user_id' => $request->user()->id,
                                    'application_id'=>$request->application_id,
                                    'severity_id'=>$request->severity_id,
                                    'location'=>$request->location,
                                    'description'=>$request->description,
                                 ]);

            $stepData = [
                'bug_id'=>$newBug->id,
                'description'=>$request->step_description,
            ];

            $step = Step::create($stepData);

            DB::commit();
            flash('Bug has been created')->success();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
            flash('Bug has not been created')->error();
        }

        return view('step.create',compact('step'));
        
    }

    public function show(Request $request, Bug $bug)
    {
        $solution = Solution::where([
                                        ['bug_id','=',$bug->id],
                                        ['user_id','=',$request->user()->id],
                                        ['status','<>','inactive']
                                    ])->first();

        return view('bug.show',compact('bug','solution'));
    }

    public function manage(Request $request, Bug $bug)
    {
        $this->authorize('manageBugStatus', Bug::class);

        return view('bug.manage',compact('bug'));
    }

    public function manageBugStatus(Request $request, Bug $bug)
    {
        $this->authorize('manageBugStatus', Bug::class);

        $bug->status = $request->status;
        $bug->save();

        flash('Bug has been '.$request->status)->success();
        return redirect()->route('bug.index');
    }

    public function edit(Request $request, Bug $bug)
    {
        $this->authorize('update', $bug);

        $applications = Application::select('id','application')
                        ->where('status','active')
                        ->orderBy('application','asc')
                        ->get();

        $severities = Severity::select('id','severity')
                      ->where('status','active')
                      ->orderBy('severity','asc')
                      ->get();

        return view('bug.edit',compact('bug','applications','severities'));
    }

    public function update(Request $request, Bug $bug)
    {
        $this->authorize('update', $bug);
        
        $bugData = $request->validate([
            'application_id'=>'required',
            'severity_id'=>'required',
            'location'=>'required|min:10|max:255',
            'description'=>'required|min:10',
        ]);

        $bug->update($bugData);

        flash('Bug has been updated')->success();
        return redirect()->route('bug.index');
    }

    public function destroy(Request $request, Bug $bug)
    {
        $this->authorize('delete',$bug);

        DB::beginTransaction();

        try {
            $steps = Step::where('bug_id','=',$bug->id);
            $steps->delete();

            $bug->delete();

            flash('Bug has been removed')->success();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
            flash('Bug has not been removed')->error();
        }

        return redirect()->route('bug.index');
    }
}
