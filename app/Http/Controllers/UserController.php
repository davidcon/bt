<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use Laracasts\Flash\Flash;
use App\Exports\UserExport;
use App\User;
use App\Position;
use App\Role;

class UserController extends Controller
{
    public function index(Request $request, User $user)
    {
        $this->authorize('viewAny', User::class);

        $users = User::all();
        return view('user.index',compact('users'));
    }

    public function userExcelExport()
    {
        $this->authorize('viewAny', User::class);
        
        $userExport = new UserExport('');
        return $userExport->download('Users.xlsx');
    }

    public function create(Request $request, User $user)
    {
        $this->authorize('create', User::class);

        $positions = Position::select('id','position')->get();
        $roles = Role::select('id','role')->get();

        return view('user.create',compact('positions','roles'));
    }

    public function store(Request $request, User $user)
    {
        $this->authorize('create', User::class);

        $userData = $request->validate([
            'role_id' => 'required',
            'position_id' => 'required',
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $userConfirmation = User::create([
            'role_id'=>$userData['role_id'],
            'position_id' => $userData['position_id'],
            'name' => $userData['name'],
            'lastname' => $userData['lastname'],
            'email' => $userData['email'],
            'password' => Hash::make($userData['password']),
        ]);

        Mail::to($userConfirmation)->send(new WelcomeMail($userConfirmation));

        flash('User has been created')->success();
        return redirect()->route('user.index');
    }

    public function edit(Request $request, User $user)
    {  
        $this->authorize('update', $request->user);

        $positions = Position::select('id','position')->where('status','active')->get();
        $roles = Role::select('id','role')->where('status','active')->get();

        return view('user.edit',compact('user','roles','positions'));
    }

    public function update(Request $request, User $user)
    {
        $this->authorize('update', $request->user);

        $userData = $request->validate([
            'role_id' => 'required',
            'position_id' => 'required',
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ]);
        
        $user->update($userData);

        flash('User has been updated')->success();

        if($request->user()->isAdmin()){
            return redirect()->route('user.index');
        }

        return redirect()->route('user.edit',compact('user'));     
    }

    public function manageUserStatus(Request $request, User $user)
    {
        $this->authorize('manageStatus', User::class);
        
        $user->status = $request->status;
        $user->save();

        flash('User has been '.$request->status)->success();
        return redirect()->route('user.index');
    }

    public function destroy(Request $request, User $user)
    {
        $this->authorize('delete', $request->user);

        $user->delete();

        flash('User has been deleted')->success();
        return redirect()->route('user.index');
    }
}
