<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Mail;
use App\Mail\SolutionMail;
use App\Exports\SolutionExport;
use App\Solution;
use App\Bug;

class SolutionController extends Controller
{

    public function index(Request $request)
    {
        $this->authorize('viewAny', Solution::class);

        if($request->user()->isAdmin()){
            $solutions = Solution::where('status','active')->get();
        }else{
            $solutions = Solution::where('user_id',$request->user()->id)->get();
        }
        
        return view('solution.index',compact('solutions'));
    }

    public function solutionExcelExport(Request $request)
    {
        if(empty($request->user)){
            if($request->user()->isAdmin()){
                $user_id = 0; 
            }else{
                return abort_if(! $request->user()->isAdmin(), 403); 
            }           
        }else{
            $user_id = $request->user()->id;
        } 
        $solutionExport = new SolutionExport('status',$user_id);
        return $solutionExport->download('Solutions.xlsx');
    }

    public function store(Request $request, Solution $solution)
    {
        $this->authorize('create', Solution::class);
        
        $solutionData = $request->validate([
            'bug_id'=>'required',
            'description'=>'required|min:50|max:250',
        ]);

        $bug = Bug::where('id','=',$request->bug_id)->first();

        $emailData = $solution->create([
            'user_id'=>$request->user()->id,
            'bug_id'=>$request->bug_id,
            'description'=>$request->description,
            'score'=> $bug->severity->score,
        ]);

        $user = $request->user();

        Mail::to($user)->send(new SolutionMail($user, $emailData));

        flash('Solution has been send')->success();
        return redirect()->route('bug.index');
    }

    public function edit(Solution $solution)
    {
        $this->authorize('update', $solution);

        return view('solution.management', compact('solution')); 
    }

    public function solutionApproved(Request $request, Solution $solution)
    {
        $this->authorize('solutionManagement', Solution::class);

        $request->validate([
            'approved'=>'required',
        ]);
        
        DB::beginTransaction();
        try {
            $solution->status = $request->approved;
            $solution->save();

            $bug = Bug::find($solution->bug_id);
            $bug->status = 'solve';
            $bug->save();
            
            flash('Solution has been '.$request->approved)->success();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
            flash('Solution has not been '.$request->approved)->error();
        }
        
        return redirect()->route('solution.index');
    }

    public function solutionRejected(Request $request, Solution $solution)
    {
        $this->authorize('solutionManagement', Solution::class);

        $request->validate([
            'rejected'=>'required',
        ]);

        $solution->status = $request->rejected;
        $solution->save();
        
        flash('Solution has been '.$request->rejected)->success();
        return redirect()->route('solution.index');
    }

    public function update(Request $request, Solution $solution)
    {
        $this->authorize('update', $solution);

        $solutionData = $request->validate([
            'description'=>'required|min:50|max:250'
        ]);

        $solution->update($solutionData);

        flash('Solution has been updated')->success();
        return redirect()->route('solution.index');
    }

    public function destroy(Solution $solution)
    {
        $this->authorize('delete', $solution);

        $solution->delete();

        flash('Solution has been deleted')->success();
        return redirect()->route('solution.index'); 
    }
}
