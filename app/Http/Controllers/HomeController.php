<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Application;
use App\Bug;
use App\Severity;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $curentDate = now();
        $startDate = now()->subMonths(5);

        $applications = Application::select('id','application')
                                     ->withCount(['bugs'])
                                     ->where('status','active')
                                     ->orderBy('bugs_count','desc')
                                     ->take(5)
                                     ->get();

        $bugs = Bug::select('id','location','created_at')
                     ->orderBy('created_at','desc')
                     ->take(5)
                     ->get();

        $severities = Severity::select('id','severity',)
                                ->whereYear('created_at',now('Y'))
                                ->withCount(['bugs'])
                                ->get();
        
        if($request->user()->isAdmin())
        {
            $scoreUsers = DB::table('solutions')
                         ->select('user_id', DB::raw('sum(score) as user_score'))
                         ->where('solutions.status','approved')
                         ->whereBetween('created_at', [$startDate, $curentDate])
                         ->groupBy('user_id')
                         ->orderBY('user_score','Desc')
                         ->take(5);

            $users = DB::table('users')
                ->joinSub($scoreUsers, 'score_users', function ($join) {
                    $join->on('users.id', '=', 'score_users.user_id');
                })
                ->select('users.id', 'users.name', 'users.lastname', 'user_score')
                ->get();
        }else{
            $scoreUsers = DB::table('solutions')
                         ->select('user_id', 
                                  DB::raw('sum(score) AS user_score'),
                                  DB::raw('YEAR(created_at) AS year, MONTH(created_at) AS month'),
                                  )
                         ->where([
                                    ['solutions.status','approved'],
                                    ['user_id','=',$request->user()->id],
                                ])
                         ->whereBetween('created_at', [$startDate, $curentDate])
                         ->groupBy('user_id', 'year', 'month')
                         ->orderBY('year', 'Desc')
                         ->orderBY('month', 'Desc');
                         
            $users = DB::table('users')
                ->joinSub($scoreUsers, 'score_users', function ($join) {
                    $join->on('users.id', '=', 'score_users.user_id');
                })
                ->select('users.id', 'users.name', 'users.lastname', 'user_score', 'year','month')
                ->get();
        }
        
        
        return view('dashboard',compact('applications','bugs','severities','users'));
    }
}
