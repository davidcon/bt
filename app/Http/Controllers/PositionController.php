<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Position;

class PositionController extends Controller
{

    public function index()
    {
        $this->authorize('viewAny', Position::class);

        $positions = Position::all();
        return view('position.index', compact('positions'));
    }

    public function store(Request $request)
    {
        $this->authorize('create', Position::class);

        $positionData = $request->validate([
            'position'=>'required|min:5',
        ]);

        Position::create($positionData);
        
        flash('Position has been created')->success();
        return redirect()->route('position.index');
    }

    public function edit(Position $position)
    {
        $this->authorize('update', $position);

        $positions = Position::all();
        return view('position.edit', compact('positions','position'));
    }

    public function update(Request $request, Position $position)
    {
        $this->authorize('update', $position);

        $positionData = $request->validate([
            'position'=>'required|min:5',
        ]);

        $position->update($positionData);

        flash('Position has been updated')->success();
        return redirect()->route('position.index');
    }

    public function destroy(Position $position)
    {
        $this->authorize('delete', $position);

        $position->delete();

        flash('Position has been deleted')->success();
        return redirect()->route('position.index');
    }
}
