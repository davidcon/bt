<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Application extends Model
{
    protected $fillable = [
        'application','description',
    ];

    protected $hidden = [
        'status',
    ];

    public function bugs(){
        return $this->hasMany('App\Bug');
    }

    public function getBugs(){
        if(Auth::user()->isAdmin()){
            return $this->bugs;
        }
        return $this->bugs->where('status','pending');
    }

    public function bugsQuantity(){
        
        if($this->bugs->count() > 0){
            return $this->bugs->count();
        }
        return 0;
    }

    public function hasBugs(){
        if($this->bugs->count() > 1){
            return true;
        }
        return false;
    }

    public function isAllowUpdate(){
        if( Auth::user()->isAdmin()){
            return true;
        }
        return false;
    }

    public function isAllowDelete(){
        if( Auth::user()->isAdmin() 
            && ($this->hasBugs() == false )){
            return true;
        }
        return false;
    }
}
