<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Solution extends Model
{
    protected $fillable = [
        'bug_id','user_id','description','score',
    ];

    protected $hidden = [
        'status',
    ];

    public function getScore()
    {
        return $this->score;
    }

    public function bug()
    {
        return $this->belongsTo('App\Bug');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getBug()
    {
       return  $this->bug->description;
    }

    public function getBugLocation()
    {
        return  $this->bug->description;
    }

    public function getApplication()
    {
        return  $this->bug->application->application;
    }

    public function getSolutionOwner()
    {
        return $this->user->name.' '.$this->user->lastname;
    }

    public function solutionManagement()
    {
        if(Auth::user()->isAdmin() && $this->status == 'active')
        {
            return true;
        }
        return false;
    }

    public function isAllowedEdit(){
        if(Auth::user()->isAdmin() || ($this->user_id === Auth::user()->id)){
            return true;
        }
        return false;
    }

    public function isAllowedDelete(){
        if(Auth::user()->isAdmin() || ($this->user_id === Auth::user()->id)){
            return true;
        }
        return false;
    }

}
