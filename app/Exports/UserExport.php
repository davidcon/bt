<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\Sheets\UserSheets;

class UserExport implements WithMultipleSheets
{
    use exportable;

    protected $status;

    public function getUserStatus()
    {
        return User::select('status')
                     ->groupBy('status')
                     ->orderBy('status','asc')
                     ->get();
    }

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function sheets(): array
    {
        $sheets = [];

        $eachUserStatus = $this->getUserStatus();

        foreach($eachUserStatus as $stat)
        {
            $sheets[] = new UserSheets($stat->status);
        }

        return $sheets;
    }
}
