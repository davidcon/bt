<?php

namespace App\Exports;

use App\Bug;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\Sheets\BugSheets;

class BugExport implements WithMultipleSheets
{
    use exportable;

    protected $status;

    public function getBugStatus()
    {
        return Bug::select('status')
                    ->groupBy('status')
                    ->orderBy('status','Asc')
                    ->get();
    }

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function sheets(): array
    {
        $sheets = [];

        $eachBugStatus = $this->getBugStatus();

        foreach ($eachBugStatus as $stat) {
            $sheets[] = new BugSheets($stat->status);
        }

        return $sheets;
    }
}
