<?php

namespace App\Exports;

use App\Application;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\Sheets\ApplicationSheets;

class ApplicationExport implements WithMultipleSheets
{
    use Exportable;

    protected $status;

    public function getApplicationStatus()
    {
        $applicationStatus = Application::select('status')
                             ->groupBy('status')
                             ->orderBy('status','Asc')
                             ->get();

        return $applicationStatus;
    }

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function sheets(): array
    {
        $sheets = [];

        $eachApplicationStatus = $this->getApplicationStatus();

        foreach($eachApplicationStatus as $stat)
        {
            $sheets[] = new ApplicationSheets($stat->status);
        }

        return $sheets;
    }
}
