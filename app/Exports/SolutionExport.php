<?php

namespace App\Exports;

use App\Solution;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\Sheets\SolutionSheets;

class SolutionExport implements WithMultipleSheets
{
    use exportable;

    protected $status;
    protected $user_id;

    public function __construct(string $status, int $user_id)
    {
        $this->status = $status;
        $this->user_id = $user_id;
    }

    public function getSolutionStatus()
    {
        
        return Solution::select('status')
                         ->groupBy('status')
                         ->orderBy('status','Asc')
                         ->get();
    }

    public function sheets(): array
    {
        $sheets = [];

        $eachSolutionStatus = $this->getSolutionStatus();

        foreach ($eachSolutionStatus as $stat) 
        {
            $sheets[] = new SolutionSheets($stat->status, $this->user_id);
        }

        return $sheets;
    }
}
