<?php

namespace App\Exports\Sheets;

use App\Bug;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class BugSheets implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    protected $status;

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function query()
    {
        return Bug::query()
                    ->where('status',$this->status);
    }

    public function title(): string
    {
        return 'Bug - '.$this->status;
    }

    public function headings(): array
    {
        return [
                'Application',
                'Bug - Location',
                'Bug - Description',
                'Severity',
                'Steps',
                'Reported By',
                'Created',
        ];
    }

    public function map($bug): array
    {
        return [
                    $bug->getApplicationName(),
                    $bug->location,
                    $bug->description,
                    $bug->getSeverity(),
                    $bug->getBugStepsQuantity(),
                    $bug->getOwnerFullname(),
                    $bug->created_at->format('Y-m-d'),
                ];
    }
}