<?php

namespace App\Exports\Sheets;

use App\User;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserSheets implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    protected $status;

    public function __construct(string $status){
        $this->status = $status;
    }

    public function query()
    {
        return User::query()
                     ->where('status',$this->status);
    }

    public function title(): string
    {
        return 'User - '.$this->status;
    }

    public function headings(): array
    {
        return [
                    'Name',
                    'Lastname',
                    'Email',
                    'Role',
                    'Position',
                    'Score',
                    'Created',
               ];
    }

    public function map($user): array
    {
        return [
                    $user->name,
                    $user->lastname,
                    $user->email,
                    $user->getRole(),
                    $user->getPosition(),
                    $user->userScore(),
                    $user->created_at->format('Y-m-d'),
                ];
    }
}