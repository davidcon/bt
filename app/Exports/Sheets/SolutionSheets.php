<?php

namespace App\Exports\Sheets;

use App\Solution;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SolutionSheets implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    protected $status;
    protected $user_id;

    public function __construct(string $status, int $user_id)
    {
        $this->status = $status;
        $this->user_id = $user_id;
    }

    public function query()
    {   
        if($this->user_id != '')
        {
            return Solution::query()
                             ->where([
                                        ['status',$this->status],
                                        ['user_id',$this->user_id]
                                    ]); 
        }
        return Solution::query()
                         ->where('status',$this->status);
    }

    public function title(): string
    {
        return "Solutions - ".$this->status;
    }

    public function headings(): array
    {
        return [
                    'Application',
                    'Bug - location',
                    'Bug - description',
                    'Solution',
                    'Score',
                    'User',
                    'Created',        
                ];
    }

    public function map($solution): array
    {
        return [
                    $solution->getApplication(),
                    $solution->getBug(),
                    $solution->getBugLocation(),
                    $solution->description,
                    $solution->score,
                    $solution->getSolutionOwner(),
                    $solution->created_at->format('Y-m-d'),
                ];
    }
}