<?php

namespace App\Exports\Sheets;

use App\Application;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class ApplicationSheets implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    protected $status;

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function query()
    {
        return Application::query()
                            ->where('status',$this->status);
    }

    public function title(): string
    {
        return 'Application - '.$this->status;
    }

    public function headings(): array
    {
        return  [
                    'Application',
                    'Description',
                    'Bug',
                    'Date',
                ];
    }

    public function map($application): array
    {
        return  [
                    $application->application,
                    $application->description,
                    $application->bugsQuantity(),
                    $application->created_at->format('Y-m-d'), 
                ];
    }

}