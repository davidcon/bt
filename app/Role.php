<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
    protected $fillable = [
        'role'
    ];

    public function users(){
        return $this->hasMany('App\User');
    }

    public function path(){
        return '/role/'.$this->id;
        //return '/role/'.$this->id.'-'.Str::slug($this->role);
    }
}
