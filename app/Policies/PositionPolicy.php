<?php

namespace App\Policies;

use App\Position;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PositionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }


    public function create(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user, Position $position)
    {
        return $user->isAdmin();
    }

    public function delete(User $user, Position $position)
    {
        return $user->isAdmin();
    }

    public function restore(User $user, Position $position)
    {
        return false;
    }

    public function forceDelete(User $user, Position $position)
    {
        return false;
    }
}
