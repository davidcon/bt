<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    public function create(User $user)
    {
        return $user->isAdmin(); 
    }

    public function update(User $user, User $userId)
    {
        return $user->isAllowEdit($userId);
    }

    public function manageStatus(User $user)
    {
        return $user->isAdmin();
    }

    public function delete(User $user, User $userId)
    {
        return $user->isAllowDelete($userId);
    }

    public function restore(User $user, User $model)
    {
        return false;
    }

    public function forceDelete(User $user, User $model)
    {
        return false;
    }
}
