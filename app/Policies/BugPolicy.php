<?php

namespace App\Policies;

use App\Bug;
use App\User;
use App\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class BugPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user)
    {
        return true;
    }

    public function bugExcelExport(User $user)
    {
        return $user->isAdmin();
    }

    public function create(User $user)
    {
        return true;
    }

    public function manageBugStatus(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user, Bug $bug)
    {
        return $bug->allowEdit(); 
    }

    public function delete(User $user, Bug $bug)
    {   
        return $bug->allowDelete();
    }

    public function restore(User $user, Bug $bug)
    {
        return false;
    }

    public function forceDelete(User $user, Bug $bug)
    {
        return false;
    }
}
