<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    public function create(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user, Role $role)
    {
        return $user->isAdmin();
    }

    public function delete(User $user, Role $role)
    {
        return $user->isAdmin();
    }

    public function restore(User $user, Role $role)
    {
        return false;
    }

    public function forceDelete(User $user, Role $role)
    {
        return false;
    }
}
