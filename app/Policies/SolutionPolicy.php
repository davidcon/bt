<?php

namespace App\Policies;

use App\Solution;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SolutionPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user, Solution $solution)
    {
        return true;
    }


    public function create(User $user)
    {
        return true;
    }

    public function update(User $user, Solution $solution)
    {
        return $solution->isAllowedEdit();
    }

    public function solutionManagement(User $user)
    {
        return $user->isAdmin();
    }


    public function delete(User $user, Solution $solution)
    {
        return $solution->isAllowedDelete();
    }

    public function restore(User $user, Solution $solution)
    {
        return false;
    }

    public function forceDelete(User $user, Solution $solution)
    {
        return false;
    }
}
