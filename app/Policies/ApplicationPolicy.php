<?php

namespace App\Policies;

use App\Application;
use App\User;
use App\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user)
    {
        return true;
    }

    public function userExcelExport(User $user)
    {
        return $user->isAdmin();
    }

    public function create(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user, Application $application)
    {
        return $application->isAllowUpdate();
    }

    public function delete(User $user, Application $application)
    {
        return $application->isAllowDelete();
    }

    public function restore(User $user, Application $application)
    {
        return false;
    }

    public function forceDelete(User $user, Application $application)
    {
        return false; 
    }
}
