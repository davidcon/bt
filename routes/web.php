<?php

use Illuminate\Support\Facades\Route;

Route::view('/','auth.login')->name('login');


Auth::routes(['register'=>false]);

Route::group(['middleware'=>'auth'],function(){
    Route::get('/home', 'HomeController@index')->name('home');

    #User Routes must be assigned to admin
    Route::get('/user','UserController@index')->name('user.index');
    Route::get('/user/create','UserController@create')->name('user.create');
    Route::get('/user/userExcelExport','UserController@userExcelExport')->name('user.userExcelExport');
    Route::post('/user','UserController@store')->name('user.store');
    Route::get('/user/{user}/edit','UserController@edit')->name('user.edit');
    Route::patch('/user/{user}','UserController@update')->name('user.update');
    Route::patch('/user/{user}/manage','UserController@manageUserStatus')->name('user.manage');
    Route::delete('/user/{user}','UserController@destroy')->name('user.delete');

    #Application Routes
    Route::get('/application','ApplicationController@index')->name('application.index');
    Route::get('/application/create','ApplicationController@create')->name('application.create');
    Route::get('/application/applicationExcelExport','ApplicationController@applicationExcelExport')->name('application.applicationExcelExport');
    Route::get('/application/{application}','ApplicationController@show')->name('application.show');
    Route::post('/application','ApplicationController@store')->name('application.store');
    Route::get('/application/{application}/edit','ApplicationController@edit')->name('application.edit');
    Route::patch('/application/{application}','ApplicationController@update')->name('application.update');
    Route::patch('/application/{application}/manage','ApplicationController@manageApplicationStatus')->name('application.manage');
    Route::delete('/application/{application}','ApplicationController@destroy')->name('application.delete');
    

    #Bug Routes
    Route::get('/bug','BugController@index')->name('bug.index');
    Route::get('/bug/create','BugController@create')->name('bug.create');
    Route::get('/bug/bugExcelExport','BugController@bugExcelExport')->name('bug.bugExcelExport');
    Route::post('/bug','BugController@store')->name('bug.store');
    Route::get('/bug/{bug}','BugController@show')->name('bug.show');
    Route::get('/bug/{bug}/manage','BugController@manage')->name('bug.manage');
    Route::get('/bug/{bug}/edit','BugController@edit')->name('bug.edit');
    Route::patch('/bug/{bug}','BugController@update')->name('bug.update');
    Route::patch('/bug/{bug}/managestatus','BugController@manageBugStatus')->name('bug.managestatus');
    Route::delete('/bug/{bug}','BugController@destroy')->name('bug.delete');

    #Role Routes must be assigned to admin
    Route::get('/role','RoleController@index')->name('role.index');
    Route::post('/role','RoleController@store')->name('role.store');
    Route::get('/role/{role}','RoleController@edit')->name('role.edit');
    Route::patch('/role/{role}','RoleController@update')->name('role.update');
    Route::delete('/role/{role}','RoleController@destroy')->name('role.delete');

    #Position Routes must be assigned to admin Role
    Route::get('/position','PositionController@index')->name('position.index');
    Route::post('/position','PositionController@store')->name('position.store');
    Route::get('/position/{position}','PositionController@edit')->name('position.edit');
    Route::patch('/position/{position}','PositionController@update')->name('position.update');
    Route::delete('/position/{position}','PositionController@destroy')->name('position.delete');

    #Severity Routes
    Route::get('/severity','SeverityController@index')->name('severity.index');
    Route::get('/severity/create','SeverityController@create')->name('severity.create');
    Route::post('/severity','SeverityController@store')->name('severity.store');
    Route::get('/severity/{severity}/edit','SeverityController@edit')->name('severity.edit');
    Route::patch('/severity/{severity}','SeverityController@update')->name('severity.update');
    Route::delete('/severity/{severity}','SeverityController@destroy')->name('severity.delete');

    #Step Route
    Route::get('/step','StepController@index')->name('step.index');
    Route::get('/step/create','StepController@create')->name('step.create');
    Route::post('/step','StepController@store')->name('step.store');

    #Solution Route
    Route::get('/solution','SolutionController@index')->name('solution.index');
    Route::post('/solution','SolutionController@store')->name('solution.store');
    Route::get('/solution/solutionExcelExport','SolutionController@solutionExcelExport')->name('solution.solutionExcelExport');
    Route::get('/solution/{solution}','SolutionController@edit')->name('solution.edit');
    Route::patch('/solution/{solution}','SolutionController@update')->name('solution.update');
    Route::patch('/solution/{solution}/management','SolutionController@solutionManagment')->name('solution.management');
    Route::patch('/solution/{solution}/approved','SolutionController@solutionApproved')->name('solution.approved');
    Route::patch('/solution/{solution}/rejected','SolutionController@solutionRejected')->name('solution.rejected');
    Route::delete('/solution/{solution}','SolutionController@destroy')->name('solution.delete');

});

